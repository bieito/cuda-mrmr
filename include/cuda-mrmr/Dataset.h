#ifndef CUDA_MRMR_DATASET_H
#define CUDA_MRMR_DATASET_H

#include <cmath>
#include <stdexcept>
#include <iostream>

template<typename T>
class Dataset {
public:
    /* Dataset operations */
    template<typename R, typename D>
    static void discretizeDataset(Dataset<R> &real_dat, Dataset<D> &disc_dat, unsigned int disc_bins = 0);

    Dataset() = default;

    inline void copy(T *raw_data, unsigned num_feat, unsigned num_samp, unsigned num_clas = 1) {
        initAllocate(num_feat, num_samp, num_clas);
        for (unsigned i = 0; i < numElements(); ++i) {
            this->raw_data[i] = raw_data[i];
        }
    }

    virtual ~Dataset() {
        if (raw_data != nullptr) {
            delete[] raw_data;
        }
    }

    inline unsigned numElements() const { return (num_feat + num_clas) * num_samp; }

    inline std::size_t numBytes() const { return numElements() * sizeof(T); }

    virtual inline void allocate() {
        if (raw_data != nullptr) {
            throw std::runtime_error("Error: Dataset: Memory already allocated");
        }
        raw_data = new T[numElements()];
    }

    inline T *feature(unsigned feature_id) const {
        if (feature_id >= num_feat) {
            throw std::invalid_argument("Error: Dataset: Feature '" + std::to_string(feature_id) + "' not in dataset");
        }
        return &raw_data[feature_id * num_samp];
    }

    inline void initAllocate(unsigned num_feat, unsigned num_samp, unsigned num_clas) {
        this->num_feat = num_feat;
        this->num_samp = num_samp;
        this->num_clas = num_clas;
        allocate();
        this->classes = &raw_data[num_feat * num_samp];
    }

    inline void print() {
        for (int i = 0; i < num_feat + 1; ++i) {
            for (int j = 0; j < num_samp; ++j) {
                std::cout << raw_data[i * num_samp + j] << " ";
            }
            std::cout << std::endl;
        }
    }

public:
    unsigned num_feat = 0;
    unsigned num_samp = 0;
    unsigned num_clas = 0;

    T *raw_data = nullptr;
    T *classes = nullptr;
};

template<typename T>
template<typename R, typename D>
void Dataset<T>::discretizeDataset(Dataset<R> &real_dat, Dataset<D> &disc_dat, unsigned int disc_bins) {

    if (real_dat.numElements() != disc_dat.numElements()) {
        throw std::invalid_argument("Error in discretizeDataset: dimensions of datasets do not match");
    }

    for (unsigned f_i = 0; f_i < real_dat.num_feat; ++f_i) {

        R *real_data = &real_dat.raw_data[f_i * real_dat.num_samp];
        D *disc_data = &disc_dat.raw_data[f_i * real_dat.num_samp];

        // First detect the minimum and maximum values
        R min_val = real_data[0];
        R max_val = real_data[0];
        for (unsigned i = 1; i < real_dat.num_samp; i++) {
            if (real_data[i] < min_val) {
                min_val = real_data[i];
            }
            if (real_data[i] > max_val) {
                max_val = real_data[i];
            }
        }

        double bin_size = (max_val - min_val) / ((double) disc_bins);
        for (unsigned i = 0; i < real_dat.num_samp; i++) {
            disc_data[i] = (D) floor((real_data[i] - min_val) / bin_size);
            /*// TODO check
            unsigned current_val = 0;
            double bin_lim = min_val + bin_size;
            while ((real_data[i] > bin_lim) && (current_val < disc_bins - 1)) {
                current_val++;
                bin_lim += bin_size;
            }
            if (disc_data[i] != current_val) {
                throw std::runtime_error("NON EQUIVALENTES");
            }*/
            /*disc_data[i] = current_val;*/
        }
    }
}

#endif //CUDA_MRMR_DATASET_H
