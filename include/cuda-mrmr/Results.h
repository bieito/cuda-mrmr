#ifndef CUDA_MRMR_RESULTS_H
#define CUDA_MRMR_RESULTS_H

#include <vector>

typedef std::vector<std::pair<unsigned, double>> Results;

#endif //CUDA_MRMR_RESULTS_H
