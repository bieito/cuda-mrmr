#ifndef CUDA_MRMR_CUDA_MRMR_H
#define CUDA_MRMR_CUDA_MRMR_H

#include "Dataset.h"
#include "Results.h"
#include "Config.h"

template<typename T>
void mrmr(unsigned k, Dataset<T> const &dataset, Results &results);

template<typename T>
void mrmr(unsigned k, Dataset<T> const &dataset, Results &results, Config &config);

/*void mrmr(unsigned k, Dataset<unsigned> const &dataset, Results &results);

void mrmr(unsigned k, Dataset<unsigned> const &dat, Results &results, Config &config);

void mrmr(unsigned k, Dataset<uint16_t> const &dataset, Results &results);

void mrmr(unsigned k, Dataset<uint16_t> const &dat, Results &results, Config &config);*/

#endif //CUDA_MRMR_CUDA_MRMR_H
