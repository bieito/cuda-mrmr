#ifndef CUDA_MRMR_ARGPARSER_H
#define CUDA_MRMR_ARGPARSER_H

#include <iomanip>
#include <iostream>
#include <map>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>
#include <optional>

using namespace std;

/**
 * @brief Argument parser for CLIs (Command Line Interfaces)
 * @details ArgParser provides a single interface to access to all CLI arguments (in a dictionary fashion), and to
 * automatically generate the usage documentation. Each argument is added with a name, which can be used to retrieve
 * its value after the parsing has been done. There are three kinds of supported arguments: Positional, Flag, Switch/Option
 */
class ArgParser {
    /**
     * Generic argument with name, description and a function to parse one or more tokens
     * And specific classes for each type
     */
    class Arg {
    public:

        /**
         * Try to parse some arguments, starting at address 0
         * @param argc number of elements in argv
         * @param argv vector of arguments
         * @return number of consumed arguments
         */
        virtual int tryParse(int argc, char *argv[]) = 0;

        /**
         * Short string to illustrate minimal usage
         * @return short string
         */
        virtual string shortOpt() const = 0;

        /**
         * Long string to illustrate all usage options
         * @return long string
         */
        virtual string longOpt() const = 0;

        /**
         * Ling string with default value (if present) and description
         * @return long string
         */
        virtual string getDesc() const = 0;

    protected:
        //string cmd_name;
        optional<string> description;
        bool parsed = false;
    };

    /**
     *
     *  Positional:
     *    - One single token
     *    - User wants to know its value
     *    - Argument determined by its position, relative to all positional arguments
     *    - Rightmost arguments may be optional (so they need a default value)
     *    - Kargs can be accepted, so instead of raising errors for unknown args, they are stored
     */
    class ArgPositional : public Arg {
    public:
        explicit ArgPositional(string param_name, optional<string> default_value = {},
                               optional<string> description = {}) : param_name(std::move(param_name)),
                                                                    default_value(std::move(default_value)) {
            this->description = std::move(description);
        }

        int tryParse(int argc, char *argv[]) override;

        string shortOpt() const override;

        string longOpt() const override;

        string getDesc() const override {
            string desc;

            if (default_value.has_value()) {
                desc = string("[default = ") + default_value.value() + string("]") +
                       (description.has_value() ? " " : "");
            }

            desc += description.value_or("");

            return desc;
        }

        string const &getValue() const {
            if (parsed) {
                return value;
            } else if (default_value.has_value()) {
                return default_value.value();
            } else {
                throw invalid_argument("Required mandatory positional argument was not found");
            }
        }

        bool isOptional() const {
            return default_value.has_value();
        }

    private:
        string value;

        string param_name;
        optional<string> default_value;
    };

    /**
     *
     *  Flag:
     *    - One single token
     *    - User wants to know if it is present or not
     *    - Argument determined by whole token
     *    - Can have short or long form:
     *        - Short starts with a single dash '-' and ends with a single character,
     *        and TODO several short flags can be merged e.g. (ls -a -l -h) ~ (ls -alh)
     *        - Long starts with a double dash '--' and ends with a n-length string
     *    - May appear more than once, but is ignored
     */
    class ArgFlag : public Arg {
    public:
        ArgFlag(vector<char> short_opt, vector<string> long_opt, optional<string> description = {}) :
                short_opt(std::move(short_opt)),
                long_opt(std::move(long_opt)) {
            this->description = std::move(description);
        }

        int tryParse(int argc, char *argv[]) override;

        string shortOpt() const override;

        string longOpt() const override;

        string getDesc() const override {
            return description.value_or("");
        }

        bool const &getValue() const {
            return parsed;
        }

        vector<char> &shortOpts() { return short_opt; }

        vector<string> &longOpts() { return long_opt; }

    private:
        vector<char> short_opt;
        vector<string> long_opt;
    };

    /**
     *
     *  Option/Switch:
     *    - Two tokens:
     *        - Separated: --verbose 3 | -V 3
     *        - TODO: Joined by equal: --verbose=3 | -V=3
     *        - TODO: Totally joined (only for short options, if used for long could cause ambiguity): -V3 | BUT --verbose3 could be a flag or an option
     *    - User wants to know its value(s)
     *    - Argument determined by first token
     *    - First token can have short or long form:
     *        - Short starts with a single dash '-' and ends with a single character
     *        - Ling starts with a double dash '--' and ends with a n-length string
     *    - May be optional (so it needs a default value)
     *    - May appear more than once
     */
    class ArgOption : public Arg {
    public:
        ArgOption(vector<char> short_opt, vector<string> long_opt, string param_name,
                  optional<string> default_value = {}, optional<string> description = {}) :
                short_opt(std::move(short_opt)),
                long_opt(std::move(long_opt)),
                param_name(std::move(param_name)),
                default_value(std::move(default_value)) {
            this->description = std::move(description);
        }

        int tryParse(int argc, char **argv) override;

        string shortOpt() const override;

        string longOpt() const override;

        string getDesc() const override {
            string desc;

            if (default_value.has_value()) {
                desc = string("[default = ") + default_value.value() + string("] ");
            }

            desc += description.value_or("");

            return desc;
        }

        vector<string> const &getValues() {
            if (parsed) {
                return value;
            } else if (default_value.has_value()) {
                value.clear();
                value.emplace_back(default_value.value());
                return value;
            } else {
                throw invalid_argument("Required mandatory switch/option argument was not parsed");
            }
        }

        vector<char> &shortOpts() { return short_opt; }

        vector<string> &longOpts() { return long_opt; }

    private:

        vector<string> value;

        vector<char> short_opt;
        vector<string> long_opt;

        string param_name;
        optional<string> default_value;
    };


public:

    ArgParser() : ArgParser("cmd", {}, false) {}

    /**
     * Create an ArgParser object with initial configuration, that is:
     * @param cmd_name name of the command/program used
     * @param cmd_description description of the command/program functionality
     * @param allow_kargs policy to handle unmatched args:
     *  - true -> unknown args can be retrieved as a vector
     *  - false -> throw exception if a unmatched argument is found
     */
    explicit ArgParser(string cmd_name, optional<string> const &cmd_description = {},
                       bool allow_kargs = false) : cmd_name(std::move(cmd_name)),
                                                   cmd_description(cmd_description),
                                                   allow_kargs(allow_kargs) {}

    virtual ~ArgParser() = default;

    /**
     * Add a positional argument
     * @param key name to reference this argument
     * @param default_value default value for arg, makes argument optional
     * @param description description to show in help message
     */
    void addPositionalArg(const string &key, string param_name, const optional<string> &default_value = {},
                          optional<string> description = {});

    /**
     * Add a flag argument
     * @param key name to reference this argument
     * @param short_opt list of characters to create one-dash-one-char option e.g. -l
     * @param long_opt list of strings to create two-dash-string option e.g. --list
     * @param description description to show in help message
     */
    void addFlagArg(const string &key, vector<char> short_opt, vector<string> long_opt,
                    optional<string> description = {});

    /**
     * Add a switch/option argument
     * @param key name to reference this argument
     * @param short_opt list of characters to create one-dash-one-char option e.g. -i file.txt || -o=file.txt
     * @param long_opt list of strings to create two-dash-string option e.g. --input file.txt || --output=file.txt
     * @param param_name name for the parameter received by the argument, usually its type (only for documentation)
     * e.g. -i {path} for -i file.txt
     * @param default_value default value for arg, makes argument optional
     * @param description description to show in help message
     */
    void addOptionArg(const string &key, vector<char> short_opt, vector<string> long_opt, string param_name,
                      optional<string> default_value = {}, optional<string> description = {});

    /**
     * Parse CLI info to internal data structure
     * @param argc number of tokens in argv
     * @param argv tokens to parse, be careful to not pass command name
     */
    void parse(int argc, char *argv[]);

    /**
     * Check if program was run with no arguments
     * @return true if no args were parsed
     */
    bool noArgs() const {
        return (parsed_args == 0);
    }

    /**
     * Get value of positional argument
     * @param key key name to reference the argument
     * @return argument value
     */
    string const &getPositionalArg(const string &key) {
        return getPositional(key).getValue();
    }

    /**
     * Check if flag was parsed
     * @param key key name to reference the argument
     * @return true if was parsed
     */
    bool isFlagPresent(const string &key) {
        return getFlag(key).getValue();
    }

    /**
     * Get first value for switch/option argument
     * @param key key name to reference the argument
     * @return argument value
     */
    string const &getOptionArg(const string &key) {
        return getOption(key).getValues().front();
    }

    /**
     * Get value(s) of switch/option argument
     * @param key key name to reference the argument
     * @return argument value(s)
     */
    vector<string> const &getOptionArgs(const string &key) {
        return getOption(key).getValues();
    }

    /**
     * Get unknown positional arguments
     * @return vector with the unknown args
     */
    vector<string> const &getKargs() {
        return k_args;
    }

    /**
     * Generate usage information from all parameters. Order is:
     *  -# Oneline usage (cmd and short versions (sorted by addition order))
     *  -# Description
     *  -# Blank line
     *  -# Description of each argument (sorted by addition order)
     * @return printable 'help' string
     */
    string helpStr() const;

    void printHelp() const {
        cout << helpStr() << endl;
    }

    explicit operator string() const { return helpStr(); }

private:
    /**
     * Throw exception if key was already added to parser
     * @param key key to check
     */
    void checkKey(string const &key) {
        if (args.find(key) != args.end()) {
            throw invalid_argument("Argument named '" + key + "' already added to parser");
        }
    }

    /**
     * Throw exception if key was not added to parser
     * @param key key to check
     */
    void validateKey(string const &key) {
        if (args.find(key) == args.end()) {
            throw invalid_argument("Argument named '" + key + "' never added to parser");
        }
    }

    /**
     * Check there is at least one short or long option to parse
     * @param key key argument name
     * @param short_opt vector of short options
     * @param long_opt vector of long options
     */
    static void checkNoEmpty(const string &key, vector<char> const &short_opt, vector<string> const &long_opt);

    /**
     * Check no short or long options have already been added to other arguments (avoid ambiguity)
     * @param key key argument name
     * @param short_opt vector of short options
     * @param long_opt vector of long options
     */
    void checkCollisions(const string &key, vector<char> const &short_opt, vector<string> const &long_opt);

    ArgPositional &getPositional(const string &key) {
        validateKey(key);
        return p_args.at(key);
    }

    ArgFlag &getFlag(const string &key) {
        validateKey(key);
        return f_args.at(key);
    }

    ArgOption &getOption(const string &key) {
        validateKey(key);
        return o_args.at(key);
    }

    string cmd_name;
    optional<string> cmd_description;
    bool allow_kargs;

    map<string, Arg &> args;
    vector<string> arg_order;
    unsigned parsed_args = 0;
    map<string, ArgPositional> p_args;
    bool allow_mandatory_positionals = true;
    map<string, ArgFlag> f_args;
    map<string, ArgOption> o_args;
    vector<string> k_args;
};

inline ostream &operator<<(ostream &strm, ArgParser const &obj) {
    return strm << (string) obj;
}

#endif //CUDA_MRMR_ARGPARSER_H
