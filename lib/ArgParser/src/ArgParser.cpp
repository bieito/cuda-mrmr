#include "ArgParser/ArgParser.h"


// ArgParser
void ArgParser::addPositionalArg(const string &key, string param_name, const optional<string> &default_value,
                                 optional<string> description) {
    checkKey(key);

    if (!default_value.has_value()) {
        // mandatory
        if (allow_mandatory_positionals) {
            // can be added (no optionals before)
            arg_order.push_back(key);
        } else {
            throw invalid_argument("Cannot add positional argument '" + key +
                                   "', mandatory arguments must appear before optional");
        }
    } else {
        // optional => add & invalidate addition of new mandatory
        arg_order.push_back(key);
        allow_mandatory_positionals = false;
    }

    p_args.insert({key, ArgPositional(std::move(param_name), default_value, std::move(description))});
    args.insert({key, p_args.at(key)});
}

void ArgParser::addFlagArg(const string &key, vector<char> short_opt, vector<string> long_opt,
                           optional<string> description) {
    checkKey(key);
    checkNoEmpty(key, short_opt, long_opt);
    checkCollisions(key, short_opt, long_opt);

    f_args.insert({key, ArgFlag(std::move(short_opt), std::move(long_opt), std::move(description))});
    args.insert({key, f_args.at(key)});
    arg_order.push_back(key);
}

void ArgParser::addOptionArg(const string &key, vector<char> short_opt, vector<string> long_opt, string param_name,
                             optional<string> default_value, optional<string> description) {
    checkKey(key);
    checkNoEmpty(key, short_opt, long_opt);
    checkCollisions(key, short_opt, long_opt);

    o_args.insert({key, ArgOption(std::move(short_opt), std::move(long_opt), std::move(param_name),
                                  std::move(default_value), std::move(description))});
    args.insert({key, o_args.at(key)});
    arg_order.push_back(key);
}

void ArgParser::parse(int argc, char *argv[]) {
    // TODO (maybe) implement with grammar (e.g. FLEX/BISON)
    // TODO (maybe) parallelize
    for (int i = 0; i < argc; ++i) {
        // Find an Arg that can parse the argument
        auto pos = arg_order.begin();
        while (pos != arg_order.end()) {
            Arg &arg = args.at(*pos);

            int consumed = arg.tryParse(argc - i, &argv[i]);

            if (consumed > 0) {
                // Parsed => stop looking to parse this argument
                i += (consumed - 1); // advance N tokens
                parsed_args += 1;
                break;
            } else {
                // Not parsed => try next
                ++pos;
            }
        }

        if (pos == arg_order.end()) {
            // Argument could not be parsed => if kargs, add to kargs, otherwise, exception
            if (allow_kargs) {
                k_args.emplace_back(argv[i]);
                parsed_args += 1;
            } else {
                throw invalid_argument("Argument '" + string(argv[i]) + "' unknown");
            }
        }

    }
}

string ArgParser::helpStr() const {
    stringstream sstr;

    sstr << "USAGE: " << cmd_name;
    for (string const &key : arg_order) {
        sstr << " " << args.at(key).shortOpt();
    }
    sstr << (allow_kargs ? " ..." : "") << endl;

    if (cmd_description.has_value()) {
        sstr << cmd_description.value() << endl;
    }

    if (!args.empty()) {
        vector<string> long_opts;
        unsigned maxlen = 0;

        for (string const &key : arg_order) {
            string long_opt = args.at(key).longOpt();
            maxlen = (long_opt.size() > maxlen) ? long_opt.size() : maxlen;
            long_opts.push_back(long_opt);
        }

        sstr << endl << "OPTIONS:" << endl;
        for (unsigned i = 0; i < long_opts.size(); ++i) {
            sstr << "  " << setw((int) maxlen + 4) << left << long_opts[i];

            // TODO (maybe) break also at screen width - total length of line is maxlen + 4 + length(current (see below))
            stringstream current_sstr(args.at(arg_order[i]).getDesc());
            string current;
            getline(current_sstr, current); // first
            sstr << current << endl;

            while (getline(current_sstr, current)) {  // find if there are more lines
                sstr << "  " << setw((int) maxlen + 8) << left << "";
                sstr << current << endl;
            }
        }
    }

    return sstr.str();
}

void ArgParser::checkNoEmpty(const string &key, vector<char> const &short_opt, vector<string> const &long_opt) {
    if (short_opt.empty() && long_opt.empty()) {
        throw invalid_argument("Argument named '" + key + "' has neither short nor long options");
    }

    for (auto &opt: long_opt) {
        if (opt.empty()) {
            throw invalid_argument("Argument named '" + key + "' has an empty option");
        }
    }
}

void ArgParser::checkCollisions(const string &key, vector<char> const &short_opt, vector<string> const &long_opt) {
    // TODO (maybe) optimize
    string coll_opt, coll_key;
    vector<char> all_short_opt;
    vector<string> all_long_opt;

    // extend all_*_opt with values from flags
    for (auto &f_arg : f_args) {
        auto tmp_short = f_arg.second.shortOpts();
        auto tmp_long = f_arg.second.longOpts();

        all_short_opt.insert(all_short_opt.end(), tmp_short.begin(), tmp_short.end());
        all_long_opt.insert(all_long_opt.end(), tmp_long.begin(), tmp_long.end());
    }
    // extend all_*_opt with values from switches/options
    for (auto &o_arg : o_args) {
        auto tmp_short = o_arg.second.shortOpts();
        auto tmp_long = o_arg.second.longOpts();

        all_short_opt.insert(all_short_opt.end(), tmp_short.begin(), tmp_short.end());
        all_long_opt.insert(all_long_opt.end(), tmp_long.begin(), tmp_long.end());
    }

    bool collided = false;
    // test short opts
    for (auto &s_opt: short_opt) {
        for (auto &x_opt: all_short_opt) {
            if (s_opt == x_opt) {
                collided = true;
                coll_opt = "short option '";
                coll_opt.push_back(s_opt);
                coll_opt += "'";
            }
        }
        if (collided) break;
    }
    // test long opts
    for (auto &l_opt: long_opt) {
        if (collided) break;
        for (auto &x_opt: all_long_opt) {
            if (l_opt == x_opt) {
                collided = true;
                coll_opt = "long option '" + l_opt + "'";
            }
        }
    }

    if (collided) {
        throw invalid_argument("Error adding argument '" + key + "': ambiguous " + coll_opt +
                               " (already defined for other argument)");
    }
}


// ArgPositional
int ArgParser::ArgPositional::tryParse(int argc, char **argv) {
    int res = 0;
    string current = string(argv[0]);

    if (argc >= 1) {
        // Positional can parse if not already parsed
        // and token does not start with '-' or "--"
        // or it is "-" or "--"
        if (!parsed && (current[0] != '-'
                        || current == string("--")
                        || current == string("-"))) {
            value = current;
            parsed = true;
            res = 1;
        }
    }

    return res;
}

string ArgParser::ArgPositional::shortOpt() const {
    string str = "<" + param_name + ">";

    if (default_value.has_value()) {
        str = "[" + str + "]";
    }

    return str;
}

string ArgParser::ArgPositional::longOpt() const {
    return string("<" + param_name + ">");
}


// ArgFlag
int ArgParser::ArgFlag::tryParse(int argc, char **argv) {
    int res = 0;
    bool ok = false;

    if (argc >= 1) {
        string current = string(argv[0]);

        // Flag does not care if already parsed, although it would be useless we have to check if is ours
        // and token can start with '-' or "--"
        if (current.size() >= 2 && current[0] == '-') {
            if (current[1] != '-' && current.size() == 2) {

                // short form i.e. -s
                for (char c : short_opt) {
                    if (c == current[1]) {
                        ok = true;
                        break;
                    }
                }

            } else if (current[1] == '-' && current.size() > 2) {

                // long form i.e. --something
                for (const string &s : long_opt) {
                    if (s == string(&current[2])) {
                        ok = true;
                        break;
                    }
                }

            }
        }

        if (ok) {
            parsed = true;
            res = 1;
        }
    }

    return res;
}

string ArgParser::ArgFlag::shortOpt() const {
    string flag;

    if (!short_opt.empty()) {
        flag = "[-";
        flag.push_back(short_opt[0]);
    } else {
        flag = "[--" + long_opt[0];
    }

    return string(flag + "]");
}

string ArgParser::ArgFlag::longOpt() const {
    vector<string> vec;
    string result;

    for (auto const &s_opt : short_opt) {
        string flag = "-";
        flag.push_back(s_opt);
        vec.push_back(flag);
    }

    for (auto const &l_opt : long_opt) {
        vec.push_back(string("--") + l_opt);
    }

    for (auto const &opt : vec) {
        result += opt + string(", ");
    }
    result.pop_back();
    result.pop_back();

    return result;
}


// ArgOption
int ArgParser::ArgOption::tryParse(int argc, char **argv) {
    int res = 0;
    bool ok = false;

    if (argc >= 2) {
        string current = string(argv[0]);

        // First token is like a flag, and second is always taken
        if (current.size() >= 2 && current[0] == '-') {
            if (current[1] != '-' && current.size() == 2) {

                // short form i.e. -s
                for (char c : short_opt) {
                    if (c == current[1]) {
                        ok = true;
                        break;
                    }
                }

            } else if (current[1] == '-' && current.size() > 2) {

                // long form i.e. --something
                for (const string &s : long_opt) {
                    if (s == string(&current[2])) {
                        ok = true;
                        break;
                    }
                }

            }
        }
    }

    if (ok) {
        value.emplace_back(argv[1]);
        parsed = true;
        res = 2;
    }

    return res;
}

string ArgParser::ArgOption::shortOpt() const {
    string flag;

    if (!short_opt.empty()) {
        flag = "-";
        flag.push_back(short_opt[0]); // priority to first option name
    } else {
        flag = "--" + long_opt[0];
    }

    flag += " <" + param_name + ">";

    if (default_value.has_value()) {
        flag = "[" + flag + "]";
    }

    return flag;
}

string ArgParser::ArgOption::longOpt() const {

    vector<string> vec;
    string result;

    for (auto const &s_opt : short_opt) {
        string flag = "-";
        flag.push_back(s_opt);
        vec.push_back(flag);
    }

    for (auto const &l_opt : long_opt) {
        vec.push_back(string("--") + l_opt);
    }

    for (auto const &opt : vec) {
        result += opt + string(", ");
    }
    result.pop_back();
    result.pop_back();

    result += " <" + param_name + ">";

    return result;

}