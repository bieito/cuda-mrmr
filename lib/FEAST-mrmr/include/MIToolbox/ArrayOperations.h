/*******************************************************************************
** ArrayOperations.h
** Part of the mutual information toolbox
**
** Contains functions to floor arrays, and to merge arrays into a joint
** state.
** 
** Author: Adam Pocock
** Created 17/2/2010
** Updated - 22/02/2014 - Added checking on calloc, and an increment array function.
**
**  Copyright 2010-2017 Adam Pocock, The University Of Manchester
**  www.cs.manchester.ac.uk
**
**  This file is part of MIToolbox, licensed under the 3-clause BSD license.
*******************************************************************************/

#ifndef __ArrayOperations_H
#define __ArrayOperations_H

#include "MIToolbox/MIToolbox.h"

#ifdef __cplusplus
extern "C" {
#endif 

/*******************************************************************************
** A version of calloc which checks to see if memory was allocated.
*******************************************************************************/
void* checkedCalloc(size_t vectorLength, size_t sizeOfType);

/*******************************************************************************
** Finds the maximum state of an int array.
*******************************************************************************/
int maxState(uint* vector, int vectorLength);

#ifdef __cplusplus
}
#endif

#endif

