/*******************************************************************************
** CalculateProbability.h
** Part of the mutual information toolbox
**
** Contains functions to calculate the probability of each state in the array
** and to calculate the probability of the joint state of two arrays
** 
** Author: Adam Pocock
** Created 17/2/2010
**
**  Copyright 2010-2017 Adam Pocock, The University Of Manchester
**  www.cs.manchester.ac.uk
**
**  This file is part of MIToolbox, licensed under the 3-clause BSD license.
*******************************************************************************/

#ifndef __CalculateProbability_H
#define __CalculateProbability_H

#include "MIToolbox/MIToolbox.h"

#ifdef __cplusplus
extern "C" {
#endif 

typedef struct jpState
{
  double *jointProbabilityVector;
  int numJointStates;
  double *firstProbabilityVector;
  int numFirstStates;
  double *secondProbabilityVector;
  int numSecondStates;
} JointProbabilityState;

/*******************************************************************************
** calculateJointProbability returns the joint probability vector of two vectors
** and the marginal probability vectors in a struct.
** It is the base operation for all information theory calculations involving 
** two or more variables.
**
** length(firstVector) == length(secondVector) == vectorLength
** otherwise it will crash
*******************************************************************************/
JointProbabilityState calculateJointProbability(uint *firstVector, uint *secondVector, int vectorLength);

/*******************************************************************************
** Frees the struct members and sets all pointers to NULL.
*******************************************************************************/
void freeJointProbabilityState(JointProbabilityState state);

#ifdef __cplusplus
}
#endif

#endif

