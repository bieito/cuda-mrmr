/*******************************************************************************
 ** ArrayOperations.c
 ** Part of the mutual information toolbox
 **
 ** Contains functions to floor arrays, and to merge arrays into a joint
 ** state.
 ** 
 ** Author: Adam Pocock
 ** Created 17/2/2010
 ** Updated - 22/02/2014 - Added checking on calloc.
 **
 ** Copyright 2010-2017 Adam Pocock, The University Of Manchester
 ** www.cs.manchester.ac.uk
 **
 ** This file is part of MIToolbox, licensed under the 3-clause BSD license.
 *******************************************************************************/

#include <errno.h>
#include "MIToolbox/MIToolbox.h"
#include "MIToolbox/ArrayOperations.h"

void* checkedCalloc(size_t vectorLength, size_t sizeOfType) {
    void *allocated = CALLOC_FUNC(vectorLength, sizeOfType);
    if(allocated == NULL) {
        fprintf(stderr, "Error: %s\nAttempted to allocate %lu length of size %lu\n", strerror(errno), vectorLength, sizeOfType);
        exit(EXIT_FAILURE);
    }
    return allocated;
}

int maxState(uint *vector, int vectorLength) {
    int i, max;
    max = 0;
    for (i = 0; i < vectorLength; i++) {
        if (vector[i] > max) {
            max = vector[i];
        }
    }
    return max + 1;
}