/*******************************************************************************
** CalculateProbability.c
** Part of the mutual information toolbox
**
** Contains functions to calculate the probability of each state in the array
** and to calculate the probability of the joint state of two arrays
** 
** Author: Adam Pocock
** Created: 17/02/2010
** Modified - 04/07/2011 - added weighted probability functions
** Updated - 22/02/2014 - Added checking on calloc.
**
** Copyright 2010-2017 Adam Pocock, The University Of Manchester
** www.cs.manchester.ac.uk
**
** This file is part of MIToolbox, licensed under the 3-clause BSD license.
*******************************************************************************/

#include "MIToolbox/MIToolbox.h"
#include "MIToolbox/ArrayOperations.h"
#include "MIToolbox/CalculateProbability.h"

JointProbabilityState calculateJointProbability(uint *firstVector, uint *secondVector, int vectorLength) {
  int *firstStateCounts;
  int *secondStateCounts;
  int *jointStateCounts;
  double *firstStateProbs;
  double *secondStateProbs;
  double *jointStateProbs;
  int firstNumStates;
  int secondNumStates;
  int jointNumStates;
  int i;
  double length = vectorLength;
  JointProbabilityState state;

  firstNumStates = maxState(firstVector,vectorLength);
  secondNumStates = maxState(secondVector,vectorLength);
  jointNumStates = firstNumStates * secondNumStates;
  
  firstStateCounts = (int *) checkedCalloc(firstNumStates,sizeof(int));
  secondStateCounts = (int *) checkedCalloc(secondNumStates,sizeof(int));
  jointStateCounts = (int *) checkedCalloc(jointNumStates,sizeof(int));
  
  firstStateProbs = (double *) checkedCalloc(firstNumStates,sizeof(double));
  secondStateProbs = (double *) checkedCalloc(secondNumStates,sizeof(double));
  jointStateProbs = (double *) checkedCalloc(jointNumStates,sizeof(double));
    
  /* Optimised for number of FP operations now O(states) instead of O(vectorLength) */
  for (i = 0; i < vectorLength; i++) {
    firstStateCounts[firstVector[i]] += 1;
    secondStateCounts[secondVector[i]] += 1;
    jointStateCounts[secondVector[i] * firstNumStates + firstVector[i]] += 1;
  }
  
  for (i = 0; i < firstNumStates; i++) {
    firstStateProbs[i] = firstStateCounts[i] / length;
  }
  
  for (i = 0; i < secondNumStates; i++) {
    secondStateProbs[i] = secondStateCounts[i] / length;
  }
  
  for (i = 0; i < jointNumStates; i++) {
    jointStateProbs[i] = jointStateCounts[i] / length;
  }

  FREE_FUNC(firstStateCounts);
  FREE_FUNC(secondStateCounts);
  FREE_FUNC(jointStateCounts);

  firstStateCounts = NULL;
  secondStateCounts = NULL;
  jointStateCounts = NULL;
  
  /*
  **typedef struct 
  **{
  **  double *jointProbabilityVector;
  **  int numJointStates;
  **  double *firstProbabilityVector;
  **  int numFirstStates;
  **  double *secondProbabilityVector;
  **  int numSecondStates;
  **} JointProbabilityState;
  */
  
  state.jointProbabilityVector = jointStateProbs;
  state.numJointStates = jointNumStates;
  state.firstProbabilityVector = firstStateProbs;
  state.numFirstStates = firstNumStates;
  state.secondProbabilityVector = secondStateProbs;
  state.numSecondStates = secondNumStates;

  return state;
}/*calcJointProbability(uint *,uint *, int)*/

void freeJointProbabilityState(JointProbabilityState state) {
    FREE_FUNC(state.firstProbabilityVector);
    state.firstProbabilityVector = NULL;
    FREE_FUNC(state.secondProbabilityVector);
    state.secondProbabilityVector = NULL;
    FREE_FUNC(state.jointProbabilityVector);
    state.jointProbabilityVector = NULL;
}
