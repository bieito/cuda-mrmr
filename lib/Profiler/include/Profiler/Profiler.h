#ifndef CUDA_MRMR_PROFILER_H
#define CUDA_MRMR_PROFILER_H

#include <chrono>

class Profiler {
public:

    typedef std::chrono::system_clock::time_point time_point;
    typedef std::chrono::microseconds::rep duration;

    Profiler() {
        this->start();
    }

    static time_point now();

    static duration from(time_point timepoint);

    static double seconds_from(time_point timepoint);

    time_point start();

    duration stop();

    duration lap();

    double stop_seconds();

    double lap_seconds();

private:
    time_point last;

};


#endif //CUDA_MRMR_PROFILER_H
