#include "Profiler/Profiler.h"

Profiler::time_point Profiler::now() {
    return std::chrono::high_resolution_clock::now();
}

Profiler::time_point Profiler::start() {
    last = Profiler::now();

    return last;
}

Profiler::duration Profiler::stop() {
    return Profiler::from(this->last);
}

Profiler::duration Profiler::lap() {
    duration d = this->stop();
    this->start();

    return d;
}

Profiler::duration Profiler::from(Profiler::time_point timepoint) {
    return std::chrono::duration_cast<std::chrono::microseconds>(Profiler::now() - timepoint).count();
}

double Profiler::seconds_from(Profiler::time_point timepoint) {
    return Profiler::from(timepoint) * 1e-6;
}

double Profiler::stop_seconds() {
    return Profiler::seconds_from(this->last);
}

double Profiler::lap_seconds() {
    double d = this->stop_seconds();
    this->start();

    return d;
}
