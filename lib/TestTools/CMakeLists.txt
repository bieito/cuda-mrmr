cmake_minimum_required(VERSION 3.15)
project(test-tools
        VERSION 0.1
        LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_FLAGS "-Wall -Wextra")
set(CMAKE_CXX_FLAGS_RELEASE "-O3")
set(CMAKE_CXX_FLAGS_DEBUG "-Og -g")

find_package(OpenMP)


################################
# LIBS
################################
add_library(test-tools STATIC src/TestTools.cpp)
target_link_libraries(test-tools PRIVATE OpenMP::OpenMP_CXX)
target_include_directories(test-tools PUBLIC include)