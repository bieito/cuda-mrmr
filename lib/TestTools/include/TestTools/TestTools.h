#ifndef CUDA_MRMR_TESTTOOLS_H
#define CUDA_MRMR_TESTTOOLS_H

#include <algorithm>
#include <utility>
#include <vector>
#include <string>
#include <exception>
#include <iostream>
#include <sstream>

/**
 * Colors for output
 */
#define RESET       "\033[0m"
#define BOLDRED     "\033[1m\033[31m"      /* Bold Red */
#define BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */
#define UBOLDYELLOW "\033[4;33m"           /* Underlined Bold Yellow */

/**
 * Shorthands for test type
 */
#define Test(NAME) void NAME ()
#define AddTest(NAME) TestManager::addTest(NAME)

typedef void (*test)();

using namespace std;

/**
 * Definition of exception for assert errors
 */
struct assertion_error : public exception {
    explicit assertion_error(string msg) : msg(std::move(msg)) {}

    inline const char *what() const noexcept override {
        return msg.c_str();
    }

private:
    string msg;
};

/**
 * Assert a given exception is not thrown
 */
#define AssertNoThrown(EXN, CODE) try { CODE } catch(EXN& err) { \
    stringstream sstr;                                           \
    sstr << UBOLDYELLOW << "AssertError: Unexpected exception catched" << RESET << " " << string(#EXN) << ": " << err.what(); \
                                                                 \
    throw assertion_error(sstr.str());                           \
}
/**
 * Assert a given exception is thrown
 */
#define AssertIsThrown(EXN, CODE) \
try {                             \
    CODE                          \
    stringstream sstr;            \
    sstr << UBOLDYELLOW << "AssertError: Expected exception not thrown" << RESET << " " << string(#EXN); \
                                  \
    throw assertion_error(sstr.str());                                                                   \
} catch(EXN&) {}

/**
 * Throw exception with message if value not true
 * @param b value that must be true
 * @param msg exception message
 */
inline void AssertTrue(bool b, const string &msg = "") {
    if (!b) {
        stringstream sstr;
        sstr << UBOLDYELLOW << "AssertError: Value not true" << RESET << (msg.empty() ? "" : " " + msg);

        throw assertion_error(sstr.str());
    }
}

/**
 * Throw exception and print values if values are different
 * @tparam T type that can be compared and converted to string
 * @param expected expected result of tested operation
 * @param actual actual result of tested operation
 */
template<typename T>
void AssertEquals(T const &expected, T const &actual) {
    if (expected != actual) {
        stringstream sstr;

        sstr << UBOLDYELLOW << "AssertError: Values not equal" << RESET << endl;
        sstr << UBOLDYELLOW << "    EXPECTED   " << RESET << endl;
        sstr << expected << endl;

        //sstr << "================================" << endl;
        sstr << UBOLDYELLOW << "     ACTUAL    " << RESET << endl;
        //sstr << "--------------------------------" << endl;
        sstr << actual;
        //sstr << "--------------------------------";

        throw assertion_error(sstr.str());
    }
}

/**
 * Static class to store and run tests
 */
class TestManager {
public:
    enum VerboseLevel {
        QUIET, REDUCED, FULL
    };

    inline static void setVerbosityLevel(VerboseLevel level) {
        verbosity = level;
    }

    /**
     * Add function to test collection
     * @param func function to use as test
     */
    inline static void addTest(test const &func) {
        if (find(tests.begin(), tests.end(), func) == tests.end()) {
            tests.push_back(func);
        } else {
            throw invalid_argument("Tried to add test twice");
        }
    }

    /**
     * Run every test and clear collection
     * @return pair of (failed, total) tests
     */
    static int runTests();

private:
    TestManager() = default;

    static vector<test> tests;
    static VerboseLevel verbosity;
};


#endif //CUDA_MRMR_TESTTOOLS_H
