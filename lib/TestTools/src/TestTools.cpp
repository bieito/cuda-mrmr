#include "TestTools/TestTools.h"

vector<test> TestManager::tests = {};
TestManager::VerboseLevel TestManager::verbosity = REDUCED;

int TestManager::runTests() {

    unsigned errors = 0;
    unsigned int align = to_string(tests.size()).size();

#pragma omp parallel for default(none) firstprivate(align) shared(cout) reduction(+: errors) schedule(dynamic)
    for (unsigned i = 0; i < tests.size(); ++i) {
        auto &test = tests[i];
        string message;
        bool error = false;

        try {
            test();
        } catch (assertion_error &err) {
            errors += 1;

            message = err.what();
            error = true;
        } catch (exception &err) {
            errors += 1;

            stringstream sstr;
            sstr << UBOLDYELLOW << "Unexpected exception" << RESET << " " << err.what();
            message = sstr.str();
            error = true;
        }

        if (verbosity > QUIET) {
#pragma omp critical
            if (error) {
                cout << "[ ";
                cout.width(align);
                cout << (i + 1) << "/" << tests.size() << "  " << BOLDRED << "FAIL" << RESET << " ] "
                     << message << RESET << endl;
            } else if (verbosity > REDUCED) {
                cout << "[ ";
                cout.width(align);
                cout << (i + 1) << "/" << tests.size() << "  ";
                cout << BOLDGREEN << "PASS" << RESET << " ]" << endl;
            }

        }
    }

    if (errors) {
        cout << errors << "/" << tests.size() << " TESTS " << BOLDRED << "FAILED" << RESET << endl;
    } else {
        cout << "ALL " << tests.size() << " TESTS " << BOLDGREEN << "PASSED" << RESET << endl;
    }

    tests.clear();

    return -((int) errors);

}