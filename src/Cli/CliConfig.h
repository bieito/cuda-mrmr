#ifndef CUDA_MRMR_CLICONFIG_H
#define CUDA_MRMR_CLICONFIG_H

#include <cuda-mrmr/Config.h>
#include <string>

class CliConfig {
public:
    enum VerboseLevel {
        QUIET, MINIMAL, FULL
    };

    enum Implementation {
        CUDA, CPU, FEAST
    };

    CliConfig() {
        mrmr_config = Config();
    }

    ~CliConfig() = default;

    unsigned int getK() const {
        return k;
    }

    std::string getDatasetPath() const {
        return input_path;
    }

    std::string getResultsPath() const {
        return output_path;
    }

    unsigned int getDiscBins() const {
        return disc_bins;
    }

    Implementation getImplementation() const {
        return implementation;
    }

    bool doValidate() const {
        return do_validate;
    }

    bool doProfile() const {
        return mrmr_config.doProfile();
    }

    VerboseLevel getVerboseLevel() const {
        return verbose_level;
    }

    Config &getmRMRConfig() {
        return mrmr_config;
    }

    void setK(unsigned int k) {
        this->k = k;
    }

    void setInputPath(const std::string &input_path) {
        this->input_path = input_path;
    }

    void setOutputPath(const std::string &output_path) {
        this->output_path = output_path;
    }

    void setDoValidate(bool do_validate) {
        this->do_validate = do_validate;
    }

    void setVerboseLevel(unsigned int verbose_level) {
        switch (verbose_level) {
            case 0:
                this->verbose_level = QUIET;
                break;
            case 1:
                this->verbose_level = MINIMAL;
                break;
            case 2:
                this->verbose_level = FULL;
                break;
            default:
                throw std::invalid_argument("Allowed verbosity levels are: 0, 1, 2");
        }
    }

    void setPrecision(const std::string &precision) {
        Config::Precision version;
        if (precision == "double") {
            version = Config::DOUBLE;
        } else if (precision == "single") {
            version = Config::SINGLE;
        } else if (precision == "fixed") {
            version = Config::FIXED; // TODO
        } else {
            throw std::invalid_argument("Precision must be one of: double, single, fixed:<bytes>");
        }
        mrmr_config.setPrecision(version);
    }

    void setImplementation(const std::string &implementation) {
        if (implementation == "cuda") {
            this->implementation = CUDA;
        } else if (implementation == "cpu") {
            this->implementation = CPU;
        } else if (implementation == "feast") {
            this->implementation = FEAST;
        } else {
            throw std::invalid_argument("Implementation must be one of: cuda, cpu, feast");
        }
    }

    void setSharedMem(bool shared_memory) {
        mrmr_config.setSharedMem(shared_memory);
    }

    void setProfile(bool profile) {
        mrmr_config.setProfile(profile);
    }

    void setGpuIndex(int gpuIndex) {
        mrmr_config.setGpuIndex(gpuIndex);
    }

    void setBlockSize(unsigned int block_size) {
        mrmr_config.setBlockSize(block_size);
    }

    void setNumStreams(unsigned int num_streams) {
        mrmr_config.setNumStreams(num_streams);
    }

    void setBatchFeatures(unsigned int batch_features) {
        mrmr_config.setBatchFeatures(batch_features);
    }

    void setDiscBins(unsigned int disc_bins) {
        this->disc_bins = disc_bins;
    }

private:
    unsigned int k;
    std::string input_path;
    std::string output_path;
    unsigned int disc_bins;
    bool no_gpu = false;
    bool do_validate = false;
    VerboseLevel verbose_level = QUIET;
    Implementation implementation = CUDA;

    Config mrmr_config; // profile
};


#endif //CUDA_MRMR_CLICONFIG_H
