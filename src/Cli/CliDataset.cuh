#ifndef CUDA_MRMR_CLIDATASET_CUH
#define CUDA_MRMR_CLIDATASET_CUH

#include <utility>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <sstream>

#include "cuda-mrmr/Dataset.h"
#include "FileParser/FileParser.h"


class CliDataset : public Dataset<unsigned> {
public:

    CliDataset() {}

    CliDataset(const std::string &file, unsigned int disc_bins);

    ~CliDataset() override;

    void allocate() override;

    inline unsigned **get2DRawData(unsigned **dst) const {
        for (unsigned i = 0; i < this->num_feat; ++i) {
            dst[i] = &this->raw_data[i * this->num_samp];
        }
        return dst;
    }

    explicit operator std::string() const;
};


std::ostream &operator<<(std::ostream &strm, CliDataset const &obj);

#endif //CUDA_MRMR_CLIDATASET_CUH
