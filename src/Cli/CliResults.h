#ifndef CUDA_MRMR_CLIRESULTS_H
#define CUDA_MRMR_CLIRESULTS_H

// CSV output
inline std::ostream &operator<<(std::ostream &strm, Results const &obj) {
    //strm << "Results: " << obj.size() << " feature" << (obj.size() != 1 ? "s" : "") << std::endl;
    strm << "#feature_index,feature_score" << std::endl;
    for (std::pair<unsigned, double> pair : obj) {
        strm << pair.first << "," << pair.second << std::endl;
    }
    return strm;
}

#endif //CUDA_MRMR_CLIRESULTS_H
