#include <iostream>
#include <chrono>
#include <thread>


extern "C" {
#include "FEAST/FSAlgorithms.h"
}

#include "ArgParser/ArgParser.h"

#include "cuda-mrmr/mrmr.h"
#include "CudaWrappers/CudaWrappers.cuh"
#include "CliDataset.cuh"
#include "CliResults.h"
#include "CliConfig.h"
#include "cpu_mrmr.h"
#include "Profiler/Profiler.h"

#include "ProjectConfig.h"

void setArgParserOptions(ArgParser &options);

bool checkAndConvertOptions(ArgParser &options, CliConfig &config);

void writeResults(Results const &results, string const &path);

void progressBarTh(const Results &results, const CliConfig &config);

void spinnerTh(const Results &results, const CliConfig &config);

void printValidationTable(const Results &feast_results, const Results &gpu_results, const CliConfig &config);


int main(int argc, char *argv[]) {

    /* 1 Parse configuration */
    ArgParser arg_parser(argv[0], "Run a CUDA accelerated version of Minimum Redundancy Maximum Relevance");
    setArgParserOptions(arg_parser);
    arg_parser.parse(argc - 1, &argv[1]);
    CliConfig config;
    if (!checkAndConvertOptions(arg_parser, config)) {
        // Graceful exit
        return 0;
    }

    /* 2 Load data */
    Profiler profiler;
    profiler.start();
    CliDataset dataset(config.getDatasetPath(), config.getDiscBins());
    if (config.doProfile()) {
        std::cout << "Profile: " << std::setprecision(6) << std::fixed << profiler.stop_seconds() << "s  Load dataset"
                  << std::endl;
    }
    if (config.getK() > dataset.num_feat) {
        std::cerr << "WARNING: Tried to select '" + to_string(config.getK()) + "' features, but dataset has only '" +
                     to_string(dataset.num_feat) + "'\n           -> using s=" + to_string(dataset.num_feat) << endl;
        config.setK(dataset.num_feat);
    }
    if (config.getVerboseLevel() == CliConfig::FULL) {
        std::cout << "Dataset: " << dataset;
    }

    /* 3 Run algorithm */
    Results gpu_results, feast_results;
    double feast_time = 1, gpu_time = 1;
    thread ui_th;
    // CUDA
    if (config.getImplementation() == CliConfig::CUDA) {
        if (config.getVerboseLevel() == CliConfig::FULL) {
            char dev_name[256];
            getCudaDefaultDeviceInfo(dev_name);
            std::cout << "Device: " << dev_name << std::endl;
        }

        if (config.doProfile()) {
            // Dont show spinner if profiling, to avoid messing up stdout
            profiler.start();
        } else {
            // This is only a thread to update an UI indicator while the master computes mrmr
            ui_th = thread(progressBarTh, std::ref(gpu_results), std::ref(config));
        }

        mrmr(config.getK(), dataset, gpu_results, config.getmRMRConfig());

        if (config.doProfile()) {
            gpu_time = profiler.stop_seconds();
            if (config.getVerboseLevel() >= CliConfig::MINIMAL) {
                std::cout << "Profile:\n";
                for (const auto &[ms, what]: config.getmRMRConfig().getProfileData()) {
                    std::cout << "         " << std::setprecision(6) << std::fixed << ms << "s  " << what << "\n";
                }
                std::cout << "         " << gpu_time << "s" << std::endl;
            } else {
                std::cout << "Profile: " << gpu_time << "s  GPU execution" << std::endl;
            }
        } else {
            ui_th.join();  // wait for ui thread to end before writing to stdout
        }

    }

    // CPU
    if (config.getImplementation() == CliConfig::CPU) {
        if (config.getVerboseLevel() == CliConfig::FULL) {
            std::cout << "Device: CPU" << std::endl;
            std::cout << "Implementation: Custom" << std::endl;
        }

        if (config.doProfile()) {
            // Dont show spinner if profiling, to avoid messing up stdout
            profiler.start();
        } else {
            // This is only a thread to update an UI indicator while the master computes mrmr
            ui_th = thread(progressBarTh, std::ref(gpu_results), std::ref(config));
        }

        CPU::mrmr(config.getK(), dataset, gpu_results, config.getmRMRConfig());

        if (config.doProfile()) {
            gpu_time = profiler.stop_seconds();
            if (config.getVerboseLevel() >= CliConfig::MINIMAL) {
                std::cout << "Profile:\n";
                for (const auto &[ms, what]: config.getmRMRConfig().getProfileData()) {
                    std::cout << "         " << std::setprecision(6) << std::fixed << ms << "s  " << what << "\n";
                }
                std::cout << "         " << gpu_time << "s" << std::endl;
            } else {
                std::cout << "Profile: " << gpu_time << "s  CPU (custom) execution" << std::endl;
            }
        } else {
            ui_th.join();  // wait for ui thread to end before writing to stdout
        }

    }

    // FEAST or VALIDATE and (CUDA or FEAST)
    if (config.getImplementation() == CliConfig::FEAST || config.doValidate()) {
        if (config.getVerboseLevel() == CliConfig::FULL) {
            std::cout << "Device: CPU" << std::endl;
            std::cout << "Implementation: FEAST" << std::endl;
        }

        // FEAST needs bi-dimensional data
        unsigned int *feature_matrix[dataset.num_feat];
        dataset.get2DRawData(feature_matrix);
        unsigned result_indexes[config.getK()];
        double result_scores[config.getK()];

        if (config.doProfile()) {
            profiler.start();
        } else {
            ui_th = thread(spinnerTh, std::ref(feast_results), std::ref(config));
        }

        mRMR_D(config.getK(), dataset.num_samp, dataset.num_feat, (unsigned **) feature_matrix,
               (unsigned *) dataset.classes,
               result_indexes, result_scores);

        if (config.doProfile()) {
            feast_time = profiler.stop_seconds();
        }

        // Copy FEAST results to vector
        for (unsigned i = 0; i < config.getK(); ++i) {
            feast_results.emplace_back(result_indexes[i], result_scores[i]);
        }

        if (config.doProfile()) {
            std::cout << "Profile: " << feast_time << "s  CPU (FEAST) execution" << std::endl;
            if (config.doValidate()) {
                std::cout << "Profile: x" << feast_time / gpu_time << "  speedup" << std::endl;
            }
        } else {
            ui_th.join();
        }

        if (config.getImplementation() != CliConfig::FEAST) {
            unsigned int num_errors = 0;
            unsigned int num_hard_errors = 0;
            double dev = 0;

            for (unsigned int i = 0; i < config.getK(); ++i) {
                if (feast_results[i].first != gpu_results[i].first) {
                    num_errors += 1;
                }

                // Find if gpu[i] is in feast[] but in other place
                unsigned int j = 0;
                while (j < config.getK() && feast_results[j].first != gpu_results[i].first) {
                    ++j;
                }
                if (j == config.getK()) {
                    num_hard_errors += 1;
                }

                dev += abs(feast_results[i].second - gpu_results[i].second);
            }

            std::cout << "Validation: " << std::endl <<
                      "  " << (num_errors == 0 ? "no" : to_string(num_errors)) << " errors (with order) out of "
                      << config.getK() << " results - total deviation " << dev << std::endl <<
                      "  " << (num_hard_errors == 0 ? "no" : to_string(num_hard_errors)) << " errors (without order)"
                      << std::endl;

            if (config.getVerboseLevel() == CliConfig::FULL) {
                printValidationTable(feast_results, gpu_results, config);
            }
        }
    }

    /* 4 Output results */
    writeResults((config.getImplementation() == CliConfig::FEAST ? feast_results : gpu_results),
                 config.getResultsPath());

}

void writeResults(Results const &results, string const &path) {

    if (path == "-") { // stdout
        std::cout << "Results: " << results.size() << " feature" << (results.size() != 1 ? "s" : "") << std::endl;
        std::cout << results;
    } else {
        ofstream file(path);

        if (file.is_open()) {
            file << results;
            file.flush();
        } else {
            throw invalid_argument("Error opening output file");
        }
    }
}

bool checkAndConvertOptions(ArgParser &options, CliConfig &config) {

    if (options.noArgs()) {
        cout << options.helpStr() << endl;
        return false;
    }

    /* First, check flags that invalidate execution */
    if (options.isFlagPresent("help")) {
        cout << options.helpStr() << endl;
        return false;
    }

    if (options.isFlagPresent("print_version")) {
        stringstream sstr;
        sstr << "mrmr-cli " << BUILD_VERSION << " (" << BUILD_TAG;
        if (!string(BUILD_TYPE).empty()) {
            sstr << "[" << BUILD_TYPE << "]";
        }
        sstr << " " << BUILD_TIME << ")" << endl;
        sstr << "[" << BUILD_CXX << ", " << BUILD_CUDA << "] on " << BUILD_PLATFORM << endl;
        cout << sstr.str();
        return false;
    }

    if (options.isFlagPresent("list_gpu")) {
        listCudaDevices();
        return false;
    }

    config.setImplementation(options.getOptionArg("implementation"));

    // TODO parse and handle fixed
    config.setPrecision(options.getOptionArg("precision"));

    if (config.getImplementation() == CliConfig::CUDA) {
        if (getCudaNumDevices() == 0) {
            throw std::runtime_error("No GPUs available, program cannot be run");
        } else {
            /* Now, check CUDA options */
            config.setBlockSize(std::stoul(options.getOptionArg("blk")));
            config.setNumStreams(std::stoul(options.getOptionArg("streams")));

            int gpu_idx = std::stoi(options.getOptionArg("gpu_index"));
            if (gpu_idx < 0 || gpu_idx > (getCudaNumDevices() - 1)) {
                throw std::invalid_argument("GPU index must be one of the listed with --list-gpus");
            }
            config.setGpuIndex(gpu_idx);

            //config.setPrecision(options.getOptionArg("precision"));
            config.setSharedMem(!options.isFlagPresent("no_shared_mem"));
        }
    }

    config.setProfile(options.isFlagPresent("profile"));

    /* Check other options for algorithm */
    try {
        config.setInputPath(options.getOptionArg("input"));
    } catch (invalid_argument &) {
        throw std::invalid_argument("Mandatory argument 'input path' not provided");
    }

    try {
        config.setOutputPath(options.getOptionArg("output"));
    } catch (invalid_argument &) {
        throw std::invalid_argument("Mandatory argument 'output path' not provided");
    }

    try {
        config.setK(std::stoul(options.getOptionArg("select")));
    } catch (invalid_argument &) {
        throw std::invalid_argument("Mandatory argument 'number of features to select' not provided");
    }

    config.setDiscBins(std::stoul(options.getOptionArg("disc_bins")));

    /* Check last configurations */
    config.setVerboseLevel(std::stoul(options.getOptionArg("verbose")));

    config.setDoValidate(options.isFlagPresent("test"));

    if (config.getImplementation() == CliConfig::FEAST && config.doValidate()) {
        config.setDoValidate(false);
        std::cerr << "WARNING: Validation is useless when running the original FEAST implementation\n"
                     "           -> Validation disabled" << endl;
    }

    if (config.getImplementation() == CliConfig::FEAST && config.getmRMRConfig().getPrecision() != Config::DOUBLE) {
        config.setPrecision("double");
        std::cerr << "WARNING: Original FEAST implementation is only compatible with double precision data types\n"
                     "           -> Switched to double precision" << endl;
    }

    /*if (config.doValidate() && config.getDataType() == CliConfig::) {
        config.setDoValidate(false);
        std::cerr << "WARNING: CPU algorithm is not compatible with half precision datatypes\n"
                     "           -> Validation disabled" << endl;
    }*/

    return true;
}

void setArgParserOptions(ArgParser &options) {
    options.addOptionArg("input", {'i'}, {"input"}, "path",
                         {}, "input file to read from");
    options.addOptionArg("output", {'o'}, {"output"}, "path",
                         {"-"}, "output file to write results to\n"""
                                "use a dash '-' for stdout");
    options.addOptionArg("select", {'n'}, {"select"}, "int",
                         {}, "number of features to select");
    options.addOptionArg("disc_bins", {'d'}, {"disc"}, "n_bins",
                         {"0"},
                         "discretize the input dataset with the binning technique using the specified number of bins\n"
                         "a value of 0 disables discretization");

    options.addOptionArg("implementation", {'r'}, {"implementation"}, "cuda|cpu|feast",
                         "cuda", "which implementation of mRMR to run\n"
                                 "cuda   CUDA version for NVIDIA GPUs - can be tuned with block-size, streams, batch, no-shmem\n" // TODO: add precision when available
                                 "cpu    custom CPU version derived from FEAST implementation, but allows for further configuration (sequential)\n"
                                 "feast  original CPU version of algorithm using the FEAST library (sequential)");

    options.addOptionArg("precision", {'p'}, {"precision"}, "double|single|fixed:<bits>",
                         {"double"}, "precision of the datatype used for the computation of scores:\n"
                                     "double        floating-point double precision (" +
                                     std::to_string(sizeof(double)) +
                                     " B), as in the original FEAST implementation\n"
                                     "single        floating-point single precision (" +
                                     std::to_string(sizeof(float)) +
                                     " B), quicker execution but lower precision in results\n"
                                     "fixed:<bits>  fixed-point with precision of specified bytes (defaults to 8 bits) (TODO: ref paper)");

    options.addOptionArg("blk", {'b'}, {"block-size", "tpb"}, "int",
                         {"512"}, "number of threads per block for CUDA kernels\n"
                                  "when shared memory usage is possible, a block of 1024 threads works best");
    options.addOptionArg("streams", {'s'}, {"streams"}, "int",
                         {"2"}, "number of streams to use to overlap memory transfers and kernels\n"
                                "recommended 2 streams for Tesla T4 or GTX 1650\n"
                                "use 0 (zero) to disable asynchronism and serialize operations");
    options.addOptionArg("batch_size", {}, {"batch"}, "int",
                         {"512"}, "approximate number of features to send in each batch of each stream\n"
                                  "higher values increase memory consumption, but lower values introduce higher overhead");
    //"works best when proportionally inverse to number of streams following the formula: batch_features = (4096 / (disc_bins/128) / num_streams^2)");
    options.addFlagArg("no_shared_mem", {}, {"no-shmem"},
                       "disable usage of shared memory (when possible) in GPU\n"
                       "this option has only interest for testing since it degrades performance");
    /*options.addOptionArg("data_type", {'t'}, {"type"}, "single|half",
                         {"single"}, "(NOT YET SUPPORTED) precision of datatype to use for dataset:\n"
                                     "single  C++ default 'unsigned int'\n"
                                     "half    half precision 'uint16_t'");*/

    options.addFlagArg("list_gpu", {}, {"list-gpu", "devices"},
                       "list CUDA enabled available GPUs");
    // TODO merge gpu and gpus options
    options.addOptionArg("gpu_index", {}, {"gpu"}, "int",
                         {std::to_string(getCudaDefaultDevice())},
            /*"(Will be deprecated in favour of --gpus) "*/"select index of GPU to use - default is system default");
    /*options.addOptionArg("gpu_indexes", {}, {"gpus"}, "all | int[,int[..]]",
                         {"all"},
                         "(NOT YET SUPPORTED) select indexes of GPUs to use for multi-gpu kernels");*/

    options.addFlagArg("test", {}, {"test", "validate"},
                       "in addition to other execution, run original FEAST implementation and validate results");
    options.addFlagArg("profile", {}, {"profile"},
                       "measure times for any active execution");

    options.addOptionArg("verbose", {'v'}, {"verbose"}, "0|1|2",
                         {"0"}, "verbosity level\n"
                                "0 - error and warning (to stderr) and explicitly wanted info as validation, profiling, etc (to stdout)\n"
                                "1 - minimal\n"
                                "2 - extra info, as dataset dimensions or device characteristics");
    options.addFlagArg("print_version",
                       {'V'}, {"version"},
                       "show the version of the program and other build info");
    options.addFlagArg("help",
                       {'h'}, {"help", "usage"},
                       "show this help and exit");
}


void progressBarTh(const Results &results, const CliConfig &config) {
    if (config.getVerboseLevel() >= CliConfig::MINIMAL) {
        const unsigned width = 40;
        int current = -1;
        auto start_time = chrono::high_resolution_clock::now();

        while (current < (int) config.getK()) {
            std::stringstream sstr;
            if ((int) results.size() > current) {
                current = results.size();
            }
            sstr << "\rProgress: [";
            for (unsigned i = 0; i < width; ++i) {
                sstr << ((i < (current * width) / config.getK()) ? "#" : " ");
            }
            auto elapsed = chrono::duration_cast<chrono::seconds>(chrono::high_resolution_clock::now() - start_time);
            sstr << "] (" << current << "/" << config.getK() << ") " << (current * 100) / config.getK() << "%" << " "
                 << elapsed.count() << "s";
            std::cout << sstr.str() << std::flush;
            std::this_thread::sleep_for(std::chrono::milliseconds(100)); // update 10 times/sec
        }
        std::cout << std::endl;
    }
}

string drawSpinner(unsigned pos, unsigned width, unsigned barsize) {
    std::stringstream sstr;
    sstr << "\rProgress: [";
    for (unsigned i = 0; i < width; ++i) {
        if ((i < pos) || (i > (pos + barsize))) {
            sstr << " ";
        } else {
            sstr << "#";
        }
    }
    sstr << "]";
    return sstr.str();
}

void spinnerTh(const Results &results, const CliConfig &config) {
    if (config.getVerboseLevel() >= CliConfig::MINIMAL) {
        const unsigned width = 40;
        unsigned barsize = width / 4;
        int current = 0;
        int direction = -1;
        auto start_time = chrono::high_resolution_clock::now();

        while (results.size() < config.getK()) {
            auto elapsed = chrono::duration_cast<chrono::seconds>(chrono::high_resolution_clock::now() - start_time);
            std::cout << drawSpinner(current, width, barsize) << " " << elapsed.count() << "s"
                      << std::flush;
            if (current < 1 || current > (int) (width - barsize - 2)) {
                direction *= -1;
            }
            current += direction;
            std::this_thread::sleep_for(std::chrono::milliseconds(100)); // update 10 times/sec
        }

        auto elapsed = chrono::duration_cast<chrono::seconds>(chrono::high_resolution_clock::now() - start_time);
        std::stringstream sstr;
        sstr << drawSpinner(0, width, width) << " (" << config.getK() << "/" << config.getK() << ") 100% "
             << elapsed.count() << "s";
        std::cout << sstr.str() << std::flush;

        std::cout << std::endl;
    }
}

void printValidationTable(const Results &feast_results, const Results &gpu_results, const CliConfig &config) {
    std::stringstream sstr;

    int lens[6] = {max(4, (int) log10(config.getK() - 1) + 1), 9, 3, max(11, 9), max(5, 9), max(6, 9)};
    for (unsigned int i = 0; i < config.getK(); ++i) {
        lens[1] = max(lens[1], (int) log10(feast_results[i].first) + 1);
        lens[2] = max(lens[2], (int) log10(gpu_results[i].first) + 1);
    }

    sstr <<
         " | " << setw(lens[0]) << "rank" <<
         " | " << setw(lens[1]) << "FEAST idx" <<
         " | " << setw(lens[2]) << "idx" <<
         " | " << setw(lens[3]) << "FEAST score" <<
         " | " << setw(lens[4]) << "score  " <<
         " | " << setw(lens[5]) << "|diff| " <<
         " | " << std::endl;

    sstr << " ";
    for (int len: lens) {
        sstr << "+";
        for (int i = 0; i < len + 2; ++i) {
            sstr << "-";
        }
    }
    sstr << "+" << std::endl;

    for (unsigned i = 0; i < config.getK(); ++i) {
        sstr <<
             " | " << setw(lens[0]) << i <<
             " | " << setw(lens[1]) << feast_results[i].first <<
             " | " << setw(lens[2]) << gpu_results[i].first <<
             " | " << setw(lens[3]) << std::fixed << feast_results[i].second <<
             " | " << setw(lens[4]) << std::fixed << gpu_results[i].second <<
             " | " << setw(lens[5]) << std::fixed << abs(feast_results[i].second - gpu_results[i].second) <<
             " | " << std::endl;
    }

    std::cout << sstr.str();
}
