#include "CudaWrappers.cuh"

void checkCudaError() {
    cudaError_t error = cudaGetLastError();
    if (error != cudaSuccess) {
        throw std::runtime_error(std::string(cudaGetErrorName(error)) + ": " + std::string(cudaGetErrorString(error)));
    }
}

void listCudaDevices() {
    int count;

    cudaGetDeviceCount(&count);
    checkCudaError();

    if (count > 0) {
        printf("CUDA Device\n");
        cudaDeviceProp devp{};
        for (int i = 0; i < count; ++i) {
            cudaGetDeviceProperties(&devp, i);
            printf("  %2d. %s [%d.%d] %dSM @ %dMHz %zuGB @ %dMHz\n", i,
                   devp.name,
                   devp.major, devp.minor,
                   devp.multiProcessorCount,
                   devp.clockRate / (int) (1e3),
                   devp.totalGlobalMem / (size_t) (1e9),
                   devp.memoryClockRate / (int) (1e3));
        }
    } else {
        printf("No CUDA Devices available\n");
    }
}

int getCudaDefaultDevice() {
    int index;
    cudaGetDevice(&index);
    checkCudaError();
    return index;
}

int getCudaNumDevices() {
    int count;
    cudaGetDeviceCount(&count);
    checkCudaError();
    return count;
}

void getCudaDefaultDeviceInfo(char buffer[]) {
    cudaDeviceProp devp{};

    cudaGetDeviceProperties(&devp, getCudaDefaultDevice());
    for (int i = 0; i < 256; ++i) {
        buffer[i] = devp.name[i];
    }
}