#ifndef CUDA_MRMR_CUDADEVICES_CUH
#define CUDA_MRMR_CUDADEVICES_CUH

#include <stdexcept>
#include <string>

#include <cstdio>

void checkCudaError();

void listCudaDevices();

int getCudaDefaultDevice();

int getCudaNumDevices();

void getCudaDefaultDeviceInfo(char buffer[]);

#endif //CUDA_MRMR_CUDADEVICES_CUH
