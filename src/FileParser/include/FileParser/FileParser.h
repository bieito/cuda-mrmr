#ifndef CUDA_MRMR_FILEPARSER_H
#define CUDA_MRMR_FILEPARSER_H

#include <fstream>
#include <type_traits>
#include <vector>

#include "cuda-mrmr/Dataset.h"

using namespace std;

class format_error : public exception {
public:
    explicit format_error(string msg) : msg(std::move(msg)) {};

    inline const char *what() const noexcept override {
        return msg.c_str();
    }

private:
    string msg;
};


class FileParser {
public:
    static FileParser *getFileParser(string const &path);

    FileParser() = default;

    explicit FileParser(string const &path) {
        file.open(path);

        if (!file.is_open()) {
            throw invalid_argument("ERROR in FileParser: file " + path + " could not be opened");
        }
    }

    virtual ~FileParser() { file.close(); }

    virtual void readInput(Dataset<unsigned> &dataset, unsigned discBins) = 0;

protected:
    static void initializeDataset(Dataset<unsigned int> &dataset, unsigned num_feat, unsigned num_samp,
                                  vector<unsigned int *> sample_data, vector<int> classes);

    static void initializeDataset(Dataset<unsigned int> &dataset, unsigned num_feat, unsigned num_samp,
                                  vector<double *> sample_data, vector<int> classes,
                                  unsigned int disc_bins);

    static void initializeDataset(Dataset<unsigned int> &dataset, unsigned num_feat, unsigned num_samp,
                                  vector<unsigned int *> sample_data, vector<int> classes,
                                  vector<unsigned int *> indexes);

    static void initializeDataset(Dataset<unsigned int> &dataset, unsigned num_feat, unsigned num_samp,
                                  vector<double *> sample_data, vector<int> classes,
                                  vector<unsigned int *> indexes,
                                  unsigned int disc_bins);

protected:
    ifstream file;
};


#endif //CUDA_MRMR_FILEPARSER_H
