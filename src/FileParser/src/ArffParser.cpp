#include <vector>

#include "ArffParser.h"

void ArffParser::readInput(Dataset<unsigned> &dataset, unsigned discBins) {

    // 0 -> Not even relation
    // 1 -> Found relation but not attributes
    // 2 -> Found attributes but not data
    // 3 -> All parts found
    int status = 0;
    string line;

    // Jump until the relation is found
    while (!file.eof() && (status == 0)) {
        getline(file, line);

        if ((line[0] != '%') && (line.length() > 0) && (line[0] != '\r')) { // Skip the empty lines and the comments
            // In this point we should find the word relation
            if ((line.compare(0, 9, "@relation") != 0) && (line.compare(0, 9, "@RELATION") != 0)) {
                throw format_error("ERROR in ArffFileParser: The first section should be relation");
            }
            status = 1;
        }
    }

    unsigned int nFeatures = 0;
    vector<NominalParameter *> nominals;

    // Count the attributes until data is found
    while (!file.eof() && (status == 1)) {
        getline(file, line);

        if ((line[0] != '%') && (line.length() > 0) && (line[0] != '\r')) { // Skip the empty lines and the comments
            if ((line.compare(0, 10, "@attribute") == 0) || (line.compare(0, 10, "@ATTRIBUTE") == 0)) {
                // Numerical value
                int discountLetters = 0;
                if (line[line.length() - 1] == '\r') {
                    discountLetters++;
                }

                if (line[line.length() - 1 - discountLetters] == ' ') {
                    discountLetters++;
                }

                if ((line.compare(line.length() - 7 - discountLetters, 7, "NUMERIC") == 0) ||
                    (line.compare(line.length() - 7 - discountLetters, 7, "numeric") == 0) ||
                    (line.compare(line.length() - 4 - discountLetters, 4, "REAL") == 0) ||
                    (line.compare(line.length() - 4 - discountLetters, 4, "real") == 0) ||
                    (line.compare(line.length() - 7 - discountLetters, 7, "INTEGER") == 0) ||
                    (line.compare(line.length() - 7 - discountLetters, 7, "integer") == 0)) {

                    nominals.push_back(nullptr);
                } else {
                    size_t posBracket = line.find('{');

                    // It is not a valid attribute
                    if (posBracket == string::npos) {
                        throw format_error("ERROR in ArffFileParser: The line " + line +
                                           " does not represent a correct attribute");
                    } else { // Nominal attribute
                        if (line[line.length() - 1] == '\r') { // Windows file with \r at the end
                            nominals.push_back(
                                    new NominalParameter(line.substr(posBracket + 1, line.length() - posBracket - 3)));
                        } else {
                            nominals.push_back(
                                    new NominalParameter(line.substr(posBracket + 1, line.length() - posBracket - 2)));
                        }
                    }
                }

                nFeatures++;
            } else if ((line == "@data") || (line == "@DATA") ||
                       (line == "@data\r") ||
                       (line == "@DATA\r")) { // The attributes have finished
                status = 2;
                nFeatures--; // The last attribute is the class
                //Utils::log("INFO in ArffFileParser: %u features and 1 class found\n", nFeatures);
            } else {
                throw format_error(
                        "ERROR in ArffFileParser: The line " + line + " when @attribute or @data is expected");
            }
        }
    }

    // Type depends on whether the input is already discrete
    vector<unsigned *> vectorU;
    vector<double *> vectorD;
    vector<int> classes;

    // Get data
    unsigned nSamples = 0;
    string auxStr;
    unsigned *samplesU;
    double *samplesD;

    while (!file.eof()) {
        getline(file, line);

        if ((line[0] != '%') && (line.length() > 0) && (line[0] != '\r')) { // Skip the empty lines and the comments
            size_t iniSub = 0;
            size_t endSub;

            if (discBins == 0) {
                samplesU = new unsigned int[nFeatures];
            } else {
                samplesD = new double[nFeatures];
            }

            for (unsigned int i = 0; i < nFeatures; i++) {
                // Get the value as a string
                endSub = line.find(',', iniSub);
                if (endSub == string::npos) {
                    throw format_error("ERROR in ArffFileParser: line " + line + " has less features than " +
                                       to_string(nFeatures));
                }
                auxStr = line.substr(iniSub, endSub - iniSub);
                iniSub = endSub + 1;

                // In case it is a number
                if (nominals[i] == nullptr) {
                    if (discBins == 0) {
                        samplesU[i] = stoul(auxStr);
                    } else {
                        samplesD[i] = stod(auxStr);
                    }
                } else {
                    unsigned int val = nominals[i]->getVal(auxStr);

                    if (discBins == 0) {
                        samplesU[i] = val;
                    } else {
                        samplesD[i] = val;
                    }
                }
            }

            if (discBins == 0) {
                vectorU.push_back(samplesU);
            } else {
                vectorD.push_back(samplesD);
            }

            // The class
            auxStr = line.substr(iniSub);

            // Remove the \r in case of windows files
            if (auxStr[auxStr.length() - 1] == '\r') {
                auxStr = auxStr.substr(0, auxStr.length() - 1);
            }

            // In case it is a number
            if (nominals[nFeatures] == nullptr) {
                classes.push_back(stoi(auxStr));
            } else {
                unsigned int val = nominals[nFeatures]->getVal(auxStr);
                classes.push_back(val);
            }

            nSamples++;
        }
    }

    //Utils::log("INFO in ArffFileParser: %u samples\n", nSamples);

    if (discBins == 0) {
        FileParser::initializeDataset(dataset, nFeatures, nSamples, vectorU, classes);
    } else {
        FileParser::initializeDataset(dataset, nFeatures, nSamples, vectorD, classes, discBins);
    }

    if (discBins == 0) {
        for (unsigned int i = 0; i < nSamples; i++) {
            delete[] vectorU[i];
        }
    } else {
        for (unsigned int i = 0; i < nSamples; i++) {
            delete[] vectorD[i];
        }
    }

    for (auto n : nominals) {
        delete n;
    }

    nominals.clear();
    classes.clear();
    vectorD.clear();
    vectorU.clear();
}

NominalParameter::NominalParameter(const string &opts) {
    options = nullptr;
    num_options = 1;

    // First, loop to check the number of options
    size_t auxPos = -1;
    while ((auxPos = opts.find(',', auxPos + 1)) != string::npos) {
        num_options++;
    }

    // Allocate the memory
    options = new string[num_options];

    // Copy the values of the strings
    int iniSub = 0;
    int endSub;
    for (unsigned i = 0; i < num_options - 1; i++) {
        endSub = opts.find(',', iniSub);
        string pattern = opts.substr(iniSub, endSub - iniSub);
        if (pattern[0] == ' ') {
            pattern = pattern.substr(1);
        }

        if (pattern[pattern.length() - 1] == ' ') {
            pattern = pattern.substr(0, pattern.length());
        }

        options[i] = pattern;

        iniSub = endSub + 1;
    }

    // Get the last one until the end
    string pattern = opts.substr(iniSub);
    if (pattern[0] == ' ') {
        pattern = pattern.substr(1);
    }

    if (pattern[pattern.length() - 1] == ' ') {
        pattern = pattern.substr(0, pattern.length() - 1);
    }

    options[num_options - 1] = pattern;
}

unsigned NominalParameter::getVal(string option) const {
    string aux = option;
    if (option[0] == ' ') {
        aux = option.substr(1);
    }

    for (unsigned i = 0; i < num_options; i++) {
        if (aux == options[i]) {
            return num_options - i - 1;
        }
    }

    // This value does not exist
    throw format_error("ERROR in NominalParameter: the option " + option + " does not exist\n");
}