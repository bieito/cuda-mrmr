#ifndef CUDA_MRMR_ARFFPARSER_H
#define CUDA_MRMR_ARFFPARSER_H

#include "FileParser/FileParser.h"

// An .arff file has the values ordered by samples, so we must mix them
class ArffParser : public FileParser {
public:
    explicit ArffParser(string const &path) : FileParser(path) {};

    ~ArffParser() override { file.close(); }

    void readInput(Dataset<unsigned> &dataset, unsigned discBins) override;
};

// Class to represent arff format nominal samples
class NominalParameter {
public:
    // The string obtained from the file with the options separated by commas
    explicit NominalParameter(const string &options);

    virtual ~NominalParameter() {
        num_options = 0;
        delete[] options;
    };

    inline unsigned getNumOptions() const { return num_options; }

    // Return the integer value for certain option
    // Otherwise, error
    unsigned getVal(string option) const;

private:
    unsigned num_options;
    string *options;
};

#endif //CUDA_MRMR_ARFFPARSER_H
