#include <vector>

#include "CsvParser.h"

void CsvParser::readInput(Dataset<unsigned> &dataset, unsigned discBins) {

    string line;
    unsigned int nFeatures = 0, nSamples = 0;

    // Type depends on whether the input is already discrete
    vector<unsigned int *> vectorU;
    vector<double *> vectorD;
    vector<int> classes;

    string auxStr;
    unsigned int *samplesU;
    double *samplesD;

    while (!file.eof()) {
        getline(file, line);

        if ((line.length() > 0) && (line[0] != '\r')) { // Skip the empty lines
            if (nFeatures == 0) { // The first row to know the number of features
                size_t auxPos = -1;
                while ((auxPos = line.find(',', auxPos + 1)) != string::npos) {
                    nFeatures++;
                }
                //Utils::log("INFO in CsvFileParser: %u features and 1 class found\n", nFeatures);
            }

            // Get the values of the line
            size_t iniSub = 0;
            size_t endSub;

            if (discBins == 0) {
                samplesU = new unsigned int[nFeatures];
            } else {
                samplesD = new double[nFeatures];
            }

            for (unsigned int i = 0; i < nFeatures; i++) {
                // Get the value as a string
                endSub = line.find(',', iniSub);
                if (endSub == string::npos) {
                    throw format_error(
                            "ERROR in CsvFileParser: line " + line + " has less features than " + to_string(nFeatures));
                }
                auxStr = line.substr(iniSub, endSub - iniSub);
                iniSub = endSub + 1;

                if (discBins == 0) {
                    samplesU[i] = stoul(auxStr);
                } else {
                    samplesD[i] = stod(auxStr);
                }
            }

            if (discBins == 0) {
                vectorU.push_back(samplesU);
            } else {
                vectorD.push_back(samplesD);
            }

            // The class
            auxStr = line.substr(iniSub);

            // Remove the \r in case of windows files
            if (auxStr[auxStr.length() - 1] == '\r') {
                auxStr = auxStr.substr(0, auxStr.length() - 1);
            }

            classes.push_back(stoi(auxStr));

            nSamples++;
        }
    }

    //Utils::log("INFO in CsvFileParser: %u samples\n", nSamples);

    if (discBins == 0) {
        FileParser::initializeDataset(dataset, nFeatures, nSamples, vectorU, classes);
    } else {
        FileParser::initializeDataset(dataset, nFeatures, nSamples, vectorD, classes, discBins);
    }

    if (discBins == 0) {
        for (unsigned int i = 0; i < nSamples; i++) {
            delete[] vectorU[i];
        }
    } else {
        for (unsigned int i = 0; i < nSamples; i++) {
            delete[] vectorD[i];
        }
    }

    classes.clear();
    vectorD.clear();
    vectorU.clear();
}