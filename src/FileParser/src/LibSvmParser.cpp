#include <vector>

#include "LibSvmParser.h"

void LibSvmParser::readInput(Dataset<unsigned> &dataset, unsigned discBins) {
    string line;
    unsigned int rowFeatures, nSamples = 0;

    // Type depends on whether the input is already discrete
    vector<unsigned int *> vectorU;
    vector<double *> vectorD;
    vector<unsigned int *> vectorIndex;
    vector<int> classes;

    string auxStr;
    unsigned int *samplesU;
    double *samplesD;
    unsigned int *indexSamples;

    // This a format that can be sparse
    // In this variable we keep the maximum number of features
    unsigned int totalFeatures = 0;

    while (!file.eof()) {
        getline(file, line);

        if ((line.length() > 0) && (line[0] != '\r')) { // Skip the empty lines and the comments
            // The number of features expressed in the current row
            rowFeatures = 0;

            size_t auxPos = -1;
            while ((auxPos = line.find(':', auxPos + 1)) != string::npos) {
                rowFeatures++;
            }

            if (discBins == 0) {
                samplesU = new unsigned int[rowFeatures];
            } else {
                samplesD = new double[rowFeatures];
            }
            // The first value of the indexes is the total number of features in the row
            indexSamples = new unsigned int[rowFeatures + 1];
            indexSamples[0] = rowFeatures;

            // First value of the row is the class
            auxStr = line.substr(0, line.find(' '));
            classes.push_back(stoi(auxStr));

            size_t iniPos = 0, endPos = 0;

            for (unsigned int i = 0; i < rowFeatures; i++) {
                endPos = line.find(':', iniPos);

                // Find the index of the sample
                iniPos = endPos - 2;
                while ((line[iniPos] != ' ') && (line[iniPos] != '\t')) {
                    iniPos--;
                }

                iniPos++;
                auxStr = line.substr(iniPos, endPos - iniPos);
                indexSamples[i + 1] = stoul(auxStr) - 1; // It starts in 1

                // Find the value of the sample
                iniPos = endPos + 1;
                while ((line[endPos] != ' ') && (line[endPos] != '\t') && (line[endPos] != '\r') &&
                       (endPos < line.length())) {
                    endPos++;
                }

                auxStr = line.substr(iniPos, endPos - iniPos);

                if (discBins == 0) {
                    samplesU[i] = stoul(auxStr);
                } else {
                    samplesD[i] = stod(auxStr);
                }
            }

            // The last index can be larger than in any previous row
            if (indexSamples[rowFeatures] + 1 > totalFeatures) {
                totalFeatures = indexSamples[rowFeatures] + 1;
            }

            if (discBins == 0) {
                vectorU.push_back(samplesU);
            } else {
                vectorD.push_back(samplesD);
            }

            vectorIndex.push_back(indexSamples);

            nSamples++;
        }
    }

    //Utils::log("INFO in LibsvmFileParser: %u features\n", totalFeatures);
    //Utils::log("INFO in LibsvmFileParser: %u samples\n", nSamples);

    if (discBins == 0) {
        FileParser::initializeDataset(dataset, totalFeatures, nSamples, vectorU, classes, vectorIndex);
    } else {
        FileParser::initializeDataset(dataset, totalFeatures, nSamples, vectorD, classes, vectorIndex, discBins);
    }

    if (discBins == 0) {
        for (unsigned int i = 0; i < nSamples; i++) {
            delete[] vectorU[i];
        }
    } else {
        for (unsigned int i = 0; i < nSamples; i++) {
            delete[] vectorD[i];
        }
    }

    for (unsigned int i = 0; i < nSamples; i++) {
        delete[] vectorIndex[i];
    }

    classes.clear();
    vectorD.clear();
    vectorU.clear();
    vectorIndex.clear();
}