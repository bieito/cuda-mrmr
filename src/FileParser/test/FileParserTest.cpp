#include "TestTools/TestTools.h"

#include "FileParser/FileParser.h"
#include "FileParserTest.h"

Test(fileParserFactoryTest) {

    /* File not found exception */
    AssertIsThrown(invalid_argument, {
        FileParser::getFileParser("nofile.arff");
    })

    /* Unknown format exception */
    AssertIsThrown(invalid_argument, {
        FileParser::getFileParser("nofile.txt");
    })

}

Test(fileParserArffTest) {
    unsigned reference[] = {0, 0, 1, 1,
                            1, 1, 0, 0,
                            1, 0, 1, 0};
    FileParser *fp;

    AssertNoThrown(invalid_argument, {
        fp = FileParser::getFileParser(string(FPTEST_PATH) + "test.arff");
    })

    Dataset<unsigned> dat;
    fp->readInput(dat, 2);
    delete fp;

    AssertEquals(2u, dat.num_feat);
    AssertEquals(4u, dat.num_samp);
    AssertEquals(1u, dat.num_clas);
    for (unsigned i = 0; i < dat.numElements(); ++i) {
        AssertEquals(reference[i], dat.raw_data[i]);
    }
}

Test(fileParserCsvTest) {
    unsigned reference[] = {0, 1, 2,
                            3, 4, 5,
                            6, 7, 8};
    FileParser *fp;

    AssertNoThrown(invalid_argument, {
        fp = FileParser::getFileParser(string(FPTEST_PATH) + "test.csv");
    })

    Dataset<unsigned> dat;
    fp->readInput(dat, 0);
    delete fp;

    AssertEquals(2u, dat.num_feat);
    AssertEquals(3u, dat.num_samp);
    AssertEquals(1u, dat.num_clas);
    for (unsigned i = 0; i < dat.numElements(); ++i) {
        AssertEquals(reference[i], dat.raw_data[i]);
    }

}

Test(fileParserLibSvmTest) {
    unsigned reference[] = {0, 0, 0, 2,
                            0, 0, 2, 0,
                            0, 0, 2, 2,
                            1, 2, 8, 9};
    FileParser *fp;

    AssertNoThrown(invalid_argument, {
        fp = FileParser::getFileParser(string(FPTEST_PATH) + "test.libsvm");
    })

    Dataset<unsigned> dat;
    fp->readInput(dat, 3);
    delete fp;

    AssertEquals(3u, dat.num_feat);
    AssertEquals(4u, dat.num_samp);
    AssertEquals(1u, dat.num_clas);
    for (unsigned i = 0; i < dat.numElements(); ++i) {
        AssertEquals(reference[i], dat.raw_data[i]);
    }

}

Test(fileParserFormatError) {
    FileParser *fp;

    fp = FileParser::getFileParser(string(FPTEST_PATH) + "test_csv.arff");

    Dataset<unsigned> dat;
    try {
        fp->readInput(dat, 3);
    } catch (format_error &err) {
        /* Expected */
        AssertEquals(string("ERROR in ArffFileParser: The first section should be relation"),
                     string(err.what()));
    }

    delete fp;

}

int main() {

    AddTest(fileParserFactoryTest);
    AddTest(fileParserArffTest);
    AddTest(fileParserCsvTest);
    AddTest(fileParserLibSvmTest);
    AddTest(fileParserFormatError);

    return TestManager::runTests();
}