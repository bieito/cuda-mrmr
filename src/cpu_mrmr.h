#ifndef CUDA_MRMR_CPU_MRMR_H
#define CUDA_MRMR_CPU_MRMR_H

#include "cuda-mrmr/Dataset.h"
#include "cuda-mrmr/Results.h"
#include "cuda-mrmr/Config.h"

#include "Profiler/Profiler.h"

#include "lut.h"

#include <chrono>
#define CPU_LOG_BASE 2

namespace CPU {
    template<typename T>
    void mrmr(unsigned k, Dataset<T> const &dataset, Results &results);

    template<typename T>
    void mrmr(unsigned k, Dataset<T> const &dataset, Results &results, Config &config);
}

#endif
