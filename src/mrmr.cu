#include <CudaWrappers/CudaWrappers.cuh>
#include "cuda-mrmr/mrmr.h"

#include "mrmr.cuh"

size_t MAX_SHARED_MEMORY_PER_BLOCK = 32 << 10;
//__device__ __constant__ uint16_t c_lut_data[32 << 10];

#if !defined(__CUDA_ARCH__) || __CUDA_ARCH__ >= 600
#else

__device__ double atomicAdd(double *address, double val) {
    auto *address_as_ull =
            (unsigned long long int *) address;
    unsigned long long int old = *address_as_ull, assumed;

    do {
        assumed = old;
        old = atomicCAS(address_as_ull, assumed,
                        __double_as_longlong(val +
                                             __longlong_as_double(assumed)));

        // Note: uses integer comparison to avoid hang in case of NaN (since NaN != NaN)
    } while (assumed != old);

    return __longlong_as_double(old);
}

#endif

__device__ uint16_t atomicAdd(uint16_t *address, uint16_t val) {
    /* If we load u16 memory as u32:
     * u16: [00] [01] [02] [03] ...
     * u32: [     00] [     01]
     * To add to the first  u16 -> + 0x0001 0000
     * To add to the second u16 -> + 0x0000 0001
     * u16 is 4B -> offset between 2 addresses is 0b0010 -> 0x0 [0000], 0x2 [0010], 0x4 [0100], 0x6 [0110] ...
     *                                                   -> xx0x (first), xx1x (second) -> mask with ~0b0010
     * u32 is 8B -> offset between 2 addresses is 0b0100 -> 0x0 [0000], 0x4 [0100], 0x8 [1000], 0xC [1100]
     * */
    auto address_as_u = (unsigned long int) address;
    auto value = (unsigned int) val;

    // We must assert we access memory in a 32bit fashion -> We have to use &[00] for both [00] or [01]
    auto *address_u32 = (unsigned int *) ((size_t) address & (~0b0011));

    if (address_as_u & 0b0010) {
        // "Lower"/First 2 bytes
        value = value << 16;
    } else {
        // "Upper"/Second 2 bytes
        value = value & 0xFFFF;
    }

    auto res = (unsigned int) atomicAdd(address_u32, value);

    if (address_as_u & 0b0010) {
        // "Lower"/First 2 bytes
        res = res >> 16;
    } else {
        // "Upper"/Second 2 bytes
    }

    return res;
}

template<typename T>
int maxState(T *vector, int vectorLength) {
    int i;
    T max = 0;
    for (i = 0; i < vectorLength; i++) {
        if (vector[i] > max) {
            max = vector[i];
        }
    }
    return max + 1;
}

template<typename D, typename R>
__global__ void
k_shared_hist2D_and_cMI_batch(D *device_data, unsigned int target_index, unsigned int *data_idxs,
                              unsigned int vectorLength, uint16_t *dataNumStates, unsigned int *d_targetStateCounts,
                              uint16_t targetNumStates, R *mutualInformation, R length) {

    extern __shared__ uint16_t data[];

    unsigned tId = threadIdx.x;
    unsigned featId = blockIdx.x;
    R partialMI = 0;
    uint16_t dNS = dataNumStates[featId];

    auto dataStateCounts = (uint16_t *) &data;
    auto jointStateCounts = (uint16_t *) &data[dNS];

    for (unsigned int pos = tId; pos < ((unsigned int) dNS * (targetNumStates + 1)); pos += blockDim.x) {
        data[pos] = 0;
    }

    // Wait for all threads to end
    __syncthreads();


    // Step 1: Count (~create histograms)
    for (unsigned int pos = tId; pos < vectorLength; pos += blockDim.x) {
        //if (tId < vectorLength) {
        D target_value = device_data[target_index * vectorLength + pos];
        D data_value = device_data[data_idxs[featId] * vectorLength + pos];

        atomicAdd(&dataStateCounts[data_value], 1);
        atomicAdd(&jointStateCounts[target_value * dNS + data_value], 1);
    }

    // Wait for all threads to end
    __syncthreads();

    // Step 2: Compute MI
    for (unsigned int pos = tId; pos < dNS; pos += blockDim.x) {
        if (dataStateCounts[pos] > 0) {
            R dSP_fI_dI = dataStateCounts[pos] * length;

            for (unsigned int targetIndex = 0; targetIndex < targetNumStates; ++targetIndex) {
                unsigned int i = targetIndex * dNS + pos;
                R jSP_fI_i = jointStateCounts[i] * length;

                if ((jSP_fI_i > 0) && (d_targetStateCounts[targetIndex] > 0)) {
                    /*double division is probably more stable than multiplying two small numbers together
                    ** mutualInformation += state.jointStateCounts[i] * log(state.jointStateCounts[i] / (state.dataStateCounts[dataIndex] * state.targetStateProbs[targetIndex]));
                    */
                    partialMI += jSP_fI_i * log2((jSP_fI_i / dSP_fI_dI) / (d_targetStateCounts[targetIndex] * length));
                }
            }
        }
    }
    if (tId < dNS) {
        atomicAdd(&mutualInformation[data_idxs[featId]], partialMI);
    }

}

template<typename T>
__global__ void
k_glob_hist2D_batch(T *device_data, unsigned int target_index, unsigned int *data_idxs, unsigned int vectorLength,
                    uint16_t *dataNumStates, uint16_t **dataStateCounts, uint16_t **jointStateCounts) {

    unsigned tId = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned featId = blockIdx.y;

    if (tId < vectorLength) {
        uint16_t dNS = dataNumStates[featId];
        T target_value = device_data[target_index * vectorLength + tId];
        T data_value = device_data[data_idxs[featId] * vectorLength + tId];

        atomicAdd(&dataStateCounts[featId][data_value], 1);
        atomicAdd(&jointStateCounts[featId][target_value * dNS + data_value], 1);
    }
}

template<typename R>
__global__ void
k_glob_cMI_batch(uint16_t **dataStateCounts, unsigned int *d_targetStateCounts, uint16_t **jointStateCounts,
                 uint16_t *dataNumStates, uint16_t targetNumStates, uint16_t *jointNumStates,
                 unsigned int *data_idxs, R *mutualInformation, R length) {

    unsigned tId = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned featId = blockIdx.y;
    R partialMI = 0;
    uint16_t dNS = dataNumStates[featId];

    if (tId < dNS && dataStateCounts[featId][tId] > 0) {
        R dSP_fI_dI = dataStateCounts[featId][tId] * length;

        for (unsigned targetIndex = 0; targetIndex < targetNumStates; ++targetIndex) {
            unsigned i = targetIndex * dNS + tId;
            R jSP_fI_i = jointStateCounts[featId][i] * length;

            if ((jSP_fI_i > 0) && (d_targetStateCounts[targetIndex] > 0)) {
                /*double division is probably more stable than multiplying two small numbers together
                ** mutualInformation += state.jointStateCounts[i] * log(state.jointStateCounts[i] / (state.dataStateCounts[dataIndex] * state.targetStateProbs[targetIndex]));
                */
                partialMI += jSP_fI_i * log2((jSP_fI_i / dSP_fI_dI) / (d_targetStateCounts[targetIndex] * length));
            }
        }

        atomicAdd(&mutualInformation[data_idxs[featId]], partialMI);
    }
}

template<typename D, typename R>
__host__ void
calcMutualInformationAll(D *host_data, D *device_data, unsigned int target_id, unsigned int vectorLength,
                         unsigned int numFeat, double *times, R *mutualInformation, Config const &config,
                         const bool *selectedFeatures,
                         unsigned int *num_states, unsigned int min_state, unsigned int max_state,
                         bool profile) {

    unsigned int blockSize = config.getBlockSize();
    dim3 blkSize(blockSize);

    Profiler profiler;
    Profiler::time_point start = profiler.start();

    //auto timepoint = std::chrono::high_resolution_clock::now();
    //auto start = std::chrono::high_resolution_clock::now();

    /* Alloc memory for results */
    R *d_mutualInformation;
    cudaMalloc(&d_mutualInformation, numFeat * sizeof(R));
    cudaMemset(d_mutualInformation, 0, numFeat * sizeof(R));

    /* Alloc and compute target feature states */
    uint16_t target_num_states = num_states[target_id];
    auto target_state_counts = new unsigned int[target_num_states]();
    unsigned int *d_target_state_counts;
    cudaMalloc(&d_target_state_counts, target_num_states * sizeof(unsigned int));
    D *targetVector = &host_data[target_id * vectorLength];
    for (unsigned int i = 0; i < vectorLength; i++) {
        target_state_counts[targetVector[i]] += 1;
    }
    cudaMemcpy(d_target_state_counts, target_state_counts, target_num_states * sizeof(unsigned int),
               cudaMemcpyHostToDevice);
    //cudaMemcpyToSymbol(d_target_state_counts, target_state_counts, target_num_states * sizeof(double));

    /* Create streams */
    bool no_async = config.getNumStreams() == 0;
    unsigned int num_streams = (no_async) ? 1 : config.getNumStreams();
    unsigned int stream_id = 0;
    cudaStream_t streams[num_streams];
    for (cudaStream_t &s: streams) {
        cudaStreamCreate(&s);
    }

    /* Calculate batch sizes */
    unsigned int f_idx = 0;
    unsigned int max_dns_sum = config.getBatchFeatures() * max_state;
    unsigned int max_feat = (max_dns_sum + min_state - 1) / min_state;

    /* Setup memory pools */
    uint16_t *d_dsc_pool, *d_jsc_pool;
    cudaMalloc(&d_jsc_pool, max_dns_sum * target_num_states * num_streams * sizeof(uint16_t));
    cudaMalloc(&d_dsc_pool, max_dns_sum * num_streams * sizeof(uint16_t));

    unsigned int *task_feats_idxs_pool; // indexes of the features in this task
    uint16_t *data_num_states_pool;
    uint16_t **d_data_state_counts_pool, **d_joint_state_counts_pool;
    cudaMallocHost(&d_data_state_counts_pool, max_feat * num_streams * sizeof(uint16_t *));
    cudaMallocHost(&d_joint_state_counts_pool, max_feat * num_streams * sizeof(uint16_t *));
    cudaMallocHost(&task_feats_idxs_pool, max_feat * num_streams * sizeof(unsigned int));
    cudaMallocHost(&data_num_states_pool, max_feat * num_streams * sizeof(uint16_t));

    unsigned int *d_task_feats_idxs_pool;
    uint16_t *d_data_num_states_pool, *d_joint_num_states_pool;
    uint16_t **d_ptr_data_sc_pool, **d_ptr_joint_sc_pool; // device pointers to the start of each feature data
    cudaMalloc(&d_ptr_data_sc_pool, max_feat * num_streams * sizeof(uint16_t *));
    cudaMalloc(&d_ptr_joint_sc_pool, max_feat * num_streams * sizeof(uint16_t *));
    cudaMalloc(&d_task_feats_idxs_pool, max_feat * num_streams * sizeof(unsigned int));
    cudaMalloc(&d_data_num_states_pool, max_feat * num_streams * sizeof(uint16_t));
    cudaMalloc(&d_joint_num_states_pool, max_feat * num_streams * sizeof(uint16_t));

    if (profile) {
        times[1] += profiler.lap_seconds();
    }

    // starting with a while loop because block factor could become dynamically adjustable
    // lets treat each iteration as a task, where we calculate the MI for *block_factor* features
    while (f_idx < numFeat) {
        if (profile && no_async) {
            profiler.start();
        }
        unsigned int stream_pos = stream_id % num_streams;
        cudaStream_t &stream = streams[stream_pos];
        ++stream_id;

        unsigned int *task_feats_idxs = &task_feats_idxs_pool[stream_pos * max_feat];
        uint16_t *data_num_states = &data_num_states_pool[stream_pos * max_feat];
        uint16_t **d_data_state_counts = &d_data_state_counts_pool[stream_pos * max_feat];
        uint16_t **d_joint_state_counts = &d_joint_state_counts_pool[stream_pos * max_feat];

        unsigned int f_count = 0; // number of features in this task -> *block_factor* except for last task // TODO must be less than 65535

        // get the next *block_factor* features that have no MI computed
        // MANDATORY synchronization -> variables of previous iteration could have not been copied to GPU yet
        cudaStreamSynchronize(stream);
        unsigned int data_num_states_sum = 0;
        while ((data_num_states_sum < max_dns_sum) && (f_idx < numFeat)) {
            if (!selectedFeatures[f_idx]) {
                unsigned int dns = num_states[f_idx];

                if (data_num_states_sum + dns > max_dns_sum) { // assert max_dns is not surpassed
                    break;
                }
                task_feats_idxs[f_count] = f_idx;
                data_num_states[f_count] = dns;
                //joint_num_states[f_count] = dns * target_num_states;
                data_num_states_sum += dns;
                ++f_count;
            }
            ++f_idx;
        }
        if (!f_count) {
            continue;
        }

        // Step 2: Allocate all memory at once
        uint16_t *d_data_num_states = &d_data_num_states_pool[stream_pos * max_feat];
        uint16_t *d_joint_num_states = &d_joint_num_states_pool[stream_pos * max_feat];
        unsigned int *d_task_feats_idxs = &d_task_feats_idxs_pool[stream_pos * max_feat];
        d_joint_state_counts[0] = &d_jsc_pool[stream_pos * max_dns_sum * target_num_states];
        d_data_state_counts[0] = &d_dsc_pool[stream_pos * max_dns_sum];
        uint16_t **d_ptr_data_sc = &d_ptr_data_sc_pool[stream_pos * max_feat];
        uint16_t **d_ptr_joint_sc = &d_ptr_joint_sc_pool[stream_pos * max_feat];

        size_t dsp_offset = 0, jsp_offset = 0;
        unsigned int max_num_states = 0;

        // Step 3: Count occurrences (create histogram) and calculate probabilities (reset memory to 0 first)
        for (unsigned int f = 0; f < f_count; ++f) {
            unsigned int ns = data_num_states[f];
            d_data_state_counts[f] = &d_data_state_counts[0][dsp_offset];
            d_joint_state_counts[f] = &d_joint_state_counts[0][jsp_offset];
            dsp_offset += ns;
            jsp_offset += ns * target_num_states;

            max_num_states = (ns > max_num_states) ? ns : max_num_states;
        }
        size_t shmem_size = (target_num_states + 1) * max_num_states * sizeof(uint16_t);
        //bool fits_in_one_block = max_num_states <= blockSize;
        bool fits_in_shared_mem = shmem_size <= MAX_SHARED_MEMORY_PER_BLOCK;
        bool use_shmem = fits_in_shared_mem && config.useSharedMem();

        if (profile && no_async) {
            times[5] += profiler.lap_seconds();
        }

        // Step 4: Copy histograms to device (all data at once)
        // array of state counts
        cudaMemcpyAsync(d_data_num_states, data_num_states, f_count * sizeof(uint16_t), cudaMemcpyHostToDevice,
                        stream);
        cudaMemcpyAsync(d_task_feats_idxs, task_feats_idxs, f_count * sizeof(unsigned int), cudaMemcpyHostToDevice,
                        stream);
        if (!use_shmem) {
            cudaMemcpyAsync(d_ptr_data_sc, d_data_state_counts, f_count * sizeof(uint16_t *), cudaMemcpyHostToDevice,
                            stream);
            cudaMemcpyAsync(d_ptr_joint_sc, d_joint_state_counts, f_count * sizeof(uint16_t *), cudaMemcpyHostToDevice,
                            stream);
            cudaMemsetAsync(d_data_state_counts[0], 0, data_num_states_sum * sizeof(uint16_t),
                            stream);
            cudaMemsetAsync(d_joint_state_counts[0], 0, data_num_states_sum * target_num_states * sizeof(uint16_t),
                            stream);
        }

        if (profile && no_async) {
            cudaStreamSynchronize(stream);
            times[6] += profiler.lap_seconds();
        }

        // Step 5: Launch kernel
        if (!use_shmem) {
            //timepoint = std::chrono::high_resolution_clock::now();

            dim3 dimGridHist2D((vectorLength + blkSize.x - 1) / blkSize.x, f_count);
            dim3 dimGridcMI((max_num_states + blkSize.x - 1) / blkSize.x, f_count);
            k_glob_hist2D_batch<D>
            <<<dimGridHist2D, blkSize, 0, stream>>>(device_data, target_id, d_task_feats_idxs, vectorLength,
                                                    d_data_num_states, d_ptr_data_sc, d_ptr_joint_sc);
            //cudaStreamSynchronize(stream);
            //times[6] += std::chrono::duration_cast<std::chrono::microseconds>(
            //        std::chrono::high_resolution_clock::now() - timepoint).count();
            //timepoint = std::chrono::high_resolution_clock::now();
            k_glob_cMI_batch<R>
            <<<dimGridcMI, blkSize, 0, stream>>>(d_ptr_data_sc, d_target_state_counts, d_ptr_joint_sc,
                                                 d_data_num_states,
                                                 target_num_states, d_joint_num_states, d_task_feats_idxs,
                                                 d_mutualInformation, (R) 1.0 / vectorLength);
            //times[7] += std::chrono::duration_cast<std::chrono::microseconds>(
            //        std::chrono::high_resolution_clock::now() - timepoint).count();
        } else {
            dim3 dimGrid(f_count);
            k_shared_hist2D_and_cMI_batch<D, R>
            <<<dimGrid, blkSize, shmem_size, stream>>>(device_data, target_id, d_task_feats_idxs, vectorLength,
                                                       d_data_num_states,
                                                       d_target_state_counts, target_num_states,
                                                       d_mutualInformation, (R) 1.0 / vectorLength);

        }

        if (profile && no_async) {
            times[7] += profiler.lap_seconds();
            cudaStreamSynchronize(stream);
            times[8] += profiler.lap_seconds();
        }

    }

    if (profile) {
        times[2] += profiler.lap_seconds();
    }

    // Copy results back
    cudaDeviceSynchronize();
    cudaError_t error = cudaGetLastError();
    if (error != cudaSuccess) {
        throw std::runtime_error(
                std::string(cudaGetErrorName(error)) + ": " + std::string(cudaGetErrorString(error)));
    }
    cudaMemcpy(mutualInformation, d_mutualInformation, numFeat * sizeof(R), cudaMemcpyDeviceToHost);

    if (profile) {
        times[3] += profiler.lap_seconds();
    }

    // Free memory
    for (cudaStream_t &s: streams) {
        cudaStreamDestroy(s);
    }

    cudaFree(d_data_num_states_pool);
    cudaFree(d_joint_num_states_pool);
    cudaFree(d_task_feats_idxs_pool);
    cudaFree(d_ptr_data_sc_pool);
    cudaFree(d_ptr_joint_sc_pool);
    cudaFreeHost(data_num_states_pool);
    cudaFreeHost(task_feats_idxs_pool);
    cudaFreeHost(d_data_state_counts_pool);
    cudaFreeHost(d_joint_state_counts_pool);
    cudaFree(d_dsc_pool);
    cudaFree(d_jsc_pool);
    delete[] target_state_counts;
    cudaFree(d_target_state_counts);
    cudaFree(d_mutualInformation);

    if (profile) {
        times[4] += profiler.lap_seconds();
        times[0] += Profiler::seconds_from(start);
    }
}

template<typename D, typename R>
void mrmr_base(unsigned k, Dataset<D> const &dat, Results &results, Config &config, D *device_data) {
    /* Array for time measurement */
    double times[16] = {0};

    /* MI values */
    R *classMI = new R[dat.num_feat]; // MI of each feature with class
    R *featureMIMatrix = new R[k * dat.num_feat]; // MI of each feature with selected

    /* Selected features map */
    auto *selectedFeatures = new bool[dat.num_feat];

    /* Cache for maxState of all features and class */
    auto *max_states = new unsigned int[dat.num_feat + 1];
    unsigned int max_state = 0, min_state = 2 << 16;
    // Compute maxState
    Profiler profiler;
    profiler.start();

#pragma omp parallel for default(none) shared(max_states, dat)
    for (unsigned int i = 0; i < dat.num_feat + 1; ++i) {
        max_states[i] = maxState(&dat.raw_data[i * dat.num_samp], dat.num_samp);
    }
#pragma omp parallel for default(none) shared(max_states, dat) reduction(min: min_state) reduction(max: max_state)
    for (unsigned int i = 0; i < dat.num_feat; ++i) {
        if (max_states[i] > max_state) {
            max_state = max_states[i];
        }
        if (max_states[i] < min_state) {
            min_state = max_states[i];
        }
    }

    if (config.doProfile()) {
        times[9] += profiler.lap_seconds();
    }

    /* Init variables */
    R maxMI = -1.0;
    unsigned int maxMICounter = 0;
    for (unsigned int i = 0; i < dat.num_feat; i++) {
        classMI[i] = 0.0;
        selectedFeatures[i] = false;
    }

    calcMutualInformationAll<D, R>(dat.raw_data, device_data, dat.num_feat, dat.num_samp, dat.num_feat, times, classMI,
                                   config, selectedFeatures, max_states, min_state, max_state, config.doProfile());
    for (unsigned int i = 0; i < dat.num_feat; i++) {
        if (classMI[i] > maxMI) {
            maxMI = classMI[i];
            maxMICounter = i;
        }/*if bigger than current maximum*/
    }/*for noOfFeatures - filling classMI*/

    selectedFeatures[maxMICounter] = true;
    results.push_back({maxMICounter, (double) maxMI});

    for (unsigned int i = 1; i < k; i++) {
        /****************************************************
        ** to ensure it selects some features
        **if this is zero then it will not pick features where the redundancy is greater than the
        **relevance
        ****************************************************/
        R score = -std::numeric_limits<R>::max();
        R totalFeatureMI;
        R currentScore = 0.0;
        unsigned int currentHighestFeature = 0;

        // First use kernel, then sum cached values
        calcMutualInformationAll<D, R>(dat.raw_data, device_data, results[i - 1].first, dat.num_samp, dat.num_feat,
                                       times, &featureMIMatrix[(i - 1) * dat.num_feat], config, selectedFeatures,
                                       max_states, min_state, max_state, config.doProfile());

        for (unsigned int j = 0; j < dat.num_feat; j++) {
            /*if we haven't selected j*/
            if (selectedFeatures[j] == 0) {
                currentScore = classMI[j];
                totalFeatureMI = 0.0;

                for (unsigned int x = 0; x < i; x++) {
                    // calcMI is only needed for the last selected (i-1) with all the not yet selected
                    totalFeatureMI += featureMIMatrix[x * dat.num_feat + j];
                }

                currentScore -= (totalFeatureMI / i);
                if (currentScore > score) {
                    score = currentScore;
                    currentHighestFeature = j;
                }
            }/*if j is unselected*/
        }/*for number of features*/

        selectedFeatures[currentHighestFeature] = true;
        results.push_back({currentHighestFeature, (double) score});

    }/*for the number of features to select*/

    if (config.doProfile()) {
        //config.appendProfileEntry(times[0] * 1e-3, "Algorithm execution");
        config.appendProfileEntry(times[9], "+-- Computation of Max States");
        config.appendProfileEntry(times[1], "|   Initial setup & Allocs");
        if (config.getNumStreams() > 0) {
            config.appendProfileEntry(times[2], "|   Quasi-Async work (transferences and kernels)");
        } else {
            config.appendProfileEntry(times[5], "|   CPU batch work");
            config.appendProfileEntry(times[6], "|   GPU Memcpy and memset");
            config.appendProfileEntry(times[7], "|   Launch kernels");
            config.appendProfileEntry(times[8], "|   Wait for kernels");
        }
        config.appendProfileEntry(times[3], "|   Wait for last kernel and copy results");
        config.appendProfileEntry(times[4], "|   Deallocs");
    }

    delete[] max_states;
    delete[] selectedFeatures;
    delete[] featureMIMatrix;
    delete[] classMI;
}

/* With Lookup Table and fixed data type ******************************************************************************/
template<typename D, typename R>
__global__ void
k_shared_hist2D_and_cMI_batch(D *device_data, unsigned int target_index, unsigned int *data_idxs,
                              unsigned int vectorLength, uint16_t *dataNumStates, unsigned int *d_targetStateCounts,
                              uint16_t targetNumStates, R *mutualInformation, R length, uint16_t *device_lut_data,
                              double lut_bf) {

    extern __shared__ uint16_t data[];

    unsigned tId = threadIdx.x;
    unsigned featId = blockIdx.x;
    R partialMI = 0;
    uint16_t dNS = dataNumStates[featId];

    auto dataStateCounts = (uint16_t *) &data;
    auto jointStateCounts = (uint16_t *) &data[dNS];

    for (unsigned int pos = tId; pos < ((unsigned int) dNS * (targetNumStates + 1)); pos += blockDim.x) {
        data[pos] = 0;
    }

    // Wait for all threads to end
    __syncthreads();


    // Step 1: Count (~create histograms)
    for (unsigned int pos = tId; pos < vectorLength; pos += blockDim.x) {
        //if (tId < vectorLength) {
        D target_value = device_data[target_index * vectorLength + pos];
        D data_value = device_data[data_idxs[featId] * vectorLength + pos];

        atomicAdd(&dataStateCounts[data_value], 1);
        atomicAdd(&jointStateCounts[target_value * dNS + data_value], 1);
    }

    // Wait for all threads to end
    __syncthreads();

    // Step 2: Compute MI
    for (unsigned int pos = tId; pos < dNS; pos += blockDim.x) {
        if (dataStateCounts[pos] > 0) {
            D dSC_fI_dI = dataStateCounts[pos];
            uint16_t dld_dSC = device_lut_data[dSC_fI_dI - 1];

            for (unsigned int targetIndex = 0; targetIndex < targetNumStates; ++targetIndex) {
                unsigned int i = targetIndex * dNS + pos;
                D jSC_fI_i = jointStateCounts[i];

                if ((jSC_fI_i > 0) && (d_targetStateCounts[targetIndex] > 0)) {
                    R jSP_fI_i = jSC_fI_i * length;
                    /*double division is probably more stable than multiplying two small numbers together
                    ** mutualInformation += state.jointStateCounts[i] * log(state.jointStateCounts[i] / (state.dataStateCounts[dataIndex] * state.targetStateProbs[targetIndex]));
                    */
                    partialMI += jSP_fI_i * ((R) (-device_lut_data[jSC_fI_i - 1] + dld_dSC +
                                                  device_lut_data[d_targetStateCounts[targetIndex] - 1])) * lut_bf;
                }
            }
        }
    }
    if (tId < dNS) {
        atomicAdd(&mutualInformation[data_idxs[featId]], partialMI);
    }

}

template<typename R>
__global__ void
k_glob_cMI_batch(uint16_t **dataStateCounts, unsigned int *d_targetStateCounts, uint16_t **jointStateCounts,
                 uint16_t *dataNumStates, uint16_t targetNumStates, uint16_t *jointNumStates,
                 unsigned int *data_idxs, R *mutualInformation, R length, uint16_t *device_lut_data,
                 double lut_bf) {

    unsigned tId = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned featId = blockIdx.y;
    R partialMI = 0;
    uint16_t dNS = dataNumStates[featId];

    if (tId < dNS && dataStateCounts[featId][tId] > 0) {
        uint16_t dSC_fI_dI = dataStateCounts[featId][tId];
        uint16_t dld_dSC = device_lut_data[dSC_fI_dI - 1];

        for (unsigned targetIndex = 0; targetIndex < targetNumStates; ++targetIndex) {
            unsigned i = targetIndex * dNS + tId;
            uint16_t jSC_fI_i = jointStateCounts[featId][i];

            if ((jSC_fI_i > 0) && (d_targetStateCounts[targetIndex] > 0)) {
                R jSP_fI_i = jSC_fI_i * length;
                /*double division is probably more stable than multiplying two small numbers together
                ** mutualInformation += state.jointStateCounts[i] * log(state.jointStateCounts[i] / (state.dataStateCounts[dataIndex] * state.targetStateProbs[targetIndex]));
                */
                partialMI += jSP_fI_i * ((R) (-device_lut_data[jSC_fI_i - 1] + dld_dSC +
                                              device_lut_data[d_targetStateCounts[targetIndex] - 1])) * lut_bf;
            }
        }

        atomicAdd(&mutualInformation[data_idxs[featId]], partialMI);
    }
}

template<typename D, typename R>
__host__ void
calcMutualInformationAll(D *host_data, D *device_data, unsigned int target_id, unsigned int vectorLength,
                         unsigned int numFeat, double *times, R *mutualInformation, Config const &config,
                         const bool *selectedFeatures,
                         unsigned int *num_states, unsigned int min_state, unsigned int max_state,
                         bool profile, uint16_t *device_lut_data, double lut_bf) {

    unsigned int blockSize = config.getBlockSize();
    dim3 blkSize(blockSize);

    Profiler profiler;
    Profiler::time_point start = profiler.start();

    //auto timepoint = std::chrono::high_resolution_clock::now();
    //auto start = std::chrono::high_resolution_clock::now();

    /* Alloc memory for results */
    R *d_mutualInformation;
    cudaMalloc(&d_mutualInformation, numFeat * sizeof(R));
    cudaMemset(d_mutualInformation, 0, numFeat * sizeof(R));

    /* Alloc and compute target feature states */
    uint16_t target_num_states = num_states[target_id];
    auto target_state_counts = new unsigned int[target_num_states]();
    unsigned int *d_target_state_counts;
    cudaMalloc(&d_target_state_counts, target_num_states * sizeof(unsigned int));
    D *targetVector = &host_data[target_id * vectorLength];
    for (unsigned int i = 0; i < vectorLength; i++) {
        target_state_counts[targetVector[i]] += 1;
    }
    cudaMemcpy(d_target_state_counts, target_state_counts, target_num_states * sizeof(unsigned int),
               cudaMemcpyHostToDevice);
    //cudaMemcpyToSymbol(d_target_state_counts, target_state_counts, target_num_states * sizeof(double));

    /* Create streams */
    bool no_async = config.getNumStreams() == 0;
    unsigned int num_streams = (no_async) ? 1 : config.getNumStreams();
    unsigned int stream_id = 0;
    cudaStream_t streams[num_streams];
    for (cudaStream_t &s: streams) {
        cudaStreamCreate(&s);
    }

    /* Calculate batch sizes */
    unsigned int f_idx = 0;
    unsigned int max_dns_sum = config.getBatchFeatures() * max_state;
    unsigned int max_feat = (max_dns_sum + min_state - 1) / min_state;

    /* Setup memory pools */
    uint16_t *d_dsc_pool, *d_jsc_pool;
    cudaMalloc(&d_jsc_pool, max_dns_sum * target_num_states * num_streams * sizeof(uint16_t));
    cudaMalloc(&d_dsc_pool, max_dns_sum * num_streams * sizeof(uint16_t));

    unsigned int *task_feats_idxs_pool; // indexes of the features in this task
    uint16_t *data_num_states_pool;
    uint16_t **d_data_state_counts_pool, **d_joint_state_counts_pool;
    cudaMallocHost(&d_data_state_counts_pool, max_feat * num_streams * sizeof(uint16_t *));
    cudaMallocHost(&d_joint_state_counts_pool, max_feat * num_streams * sizeof(uint16_t *));
    cudaMallocHost(&task_feats_idxs_pool, max_feat * num_streams * sizeof(unsigned int));
    cudaMallocHost(&data_num_states_pool, max_feat * num_streams * sizeof(uint16_t));

    unsigned int *d_task_feats_idxs_pool;
    uint16_t *d_data_num_states_pool, *d_joint_num_states_pool;
    uint16_t **d_ptr_data_sc_pool, **d_ptr_joint_sc_pool; // device pointers to the start of each feature data
    cudaMalloc(&d_ptr_data_sc_pool, max_feat * num_streams * sizeof(uint16_t *));
    cudaMalloc(&d_ptr_joint_sc_pool, max_feat * num_streams * sizeof(uint16_t *));
    cudaMalloc(&d_task_feats_idxs_pool, max_feat * num_streams * sizeof(unsigned int));
    cudaMalloc(&d_data_num_states_pool, max_feat * num_streams * sizeof(uint16_t));
    cudaMalloc(&d_joint_num_states_pool, max_feat * num_streams * sizeof(uint16_t));

    if (profile) {
        times[1] += profiler.lap_seconds();
    }

    // starting with a while loop because block factor could become dynamically adjustable
    // lets treat each iteration as a task, where we calculate the MI for *block_factor* features
    while (f_idx < numFeat) {
        if (profile && no_async) {
            profiler.start();
        }
        unsigned int stream_pos = stream_id % num_streams;
        cudaStream_t &stream = streams[stream_pos];
        ++stream_id;

        unsigned int *task_feats_idxs = &task_feats_idxs_pool[stream_pos * max_feat];
        uint16_t *data_num_states = &data_num_states_pool[stream_pos * max_feat];
        uint16_t **d_data_state_counts = &d_data_state_counts_pool[stream_pos * max_feat];
        uint16_t **d_joint_state_counts = &d_joint_state_counts_pool[stream_pos * max_feat];

        unsigned int f_count = 0; // number of features in this task -> *block_factor* except for last task // TODO must be less than 65535

        // get the next *block_factor* features that have no MI computed
        // MANDATORY synchronization -> variables of previous iteration could have not been copied to GPU yet
        cudaStreamSynchronize(stream);
        unsigned int data_num_states_sum = 0;
        while ((data_num_states_sum < max_dns_sum) && (f_idx < numFeat)) {
            if (!selectedFeatures[f_idx]) {
                unsigned int dns = num_states[f_idx];

                if (data_num_states_sum + dns > max_dns_sum) { // assert max_dns is not surpassed
                    break;
                }
                task_feats_idxs[f_count] = f_idx;
                data_num_states[f_count] = dns;
                //joint_num_states[f_count] = dns * target_num_states;
                data_num_states_sum += dns;
                ++f_count;
            }
            ++f_idx;
        }
        if (!f_count) {
            continue;
        }

        // Step 2: Allocate all memory at once
        uint16_t *d_data_num_states = &d_data_num_states_pool[stream_pos * max_feat];
        uint16_t *d_joint_num_states = &d_joint_num_states_pool[stream_pos * max_feat];
        unsigned int *d_task_feats_idxs = &d_task_feats_idxs_pool[stream_pos * max_feat];
        d_joint_state_counts[0] = &d_jsc_pool[stream_pos * max_dns_sum * target_num_states];
        d_data_state_counts[0] = &d_dsc_pool[stream_pos * max_dns_sum];
        uint16_t **d_ptr_data_sc = &d_ptr_data_sc_pool[stream_pos * max_feat];
        uint16_t **d_ptr_joint_sc = &d_ptr_joint_sc_pool[stream_pos * max_feat];

        size_t dsp_offset = 0, jsp_offset = 0;
        unsigned int max_num_states = 0;

        // Step 3: Count occurrences (create histogram) and calculate probabilities (reset memory to 0 first)
        for (unsigned int f = 0; f < f_count; ++f) {
            unsigned int ns = data_num_states[f];
            d_data_state_counts[f] = &d_data_state_counts[0][dsp_offset];
            d_joint_state_counts[f] = &d_joint_state_counts[0][jsp_offset];
            dsp_offset += ns;
            jsp_offset += ns * target_num_states;

            max_num_states = (ns > max_num_states) ? ns : max_num_states;
        }
        size_t shmem_size = (target_num_states + 1) * max_num_states * sizeof(uint16_t);
        //bool fits_in_one_block = max_num_states <= blockSize;
        bool fits_in_shared_mem = shmem_size <= MAX_SHARED_MEMORY_PER_BLOCK;
        bool use_shmem = fits_in_shared_mem && config.useSharedMem();

        if (profile && no_async) {
            times[5] += profiler.lap_seconds();
        }

        // Step 4: Copy histograms to device (all data at once)
        // array of state counts
        cudaMemcpyAsync(d_data_num_states, data_num_states, f_count * sizeof(uint16_t), cudaMemcpyHostToDevice,
                        stream);
        cudaMemcpyAsync(d_task_feats_idxs, task_feats_idxs, f_count * sizeof(unsigned int), cudaMemcpyHostToDevice,
                        stream);
        if (!use_shmem) {
            cudaMemcpyAsync(d_ptr_data_sc, d_data_state_counts, f_count * sizeof(uint16_t *), cudaMemcpyHostToDevice,
                            stream);
            cudaMemcpyAsync(d_ptr_joint_sc, d_joint_state_counts, f_count * sizeof(uint16_t *), cudaMemcpyHostToDevice,
                            stream);
            cudaMemsetAsync(d_data_state_counts[0], 0, data_num_states_sum * sizeof(uint16_t),
                            stream);
            cudaMemsetAsync(d_joint_state_counts[0], 0, data_num_states_sum * target_num_states * sizeof(uint16_t),
                            stream);
        }

        if (profile && no_async) {
            cudaStreamSynchronize(stream);
            times[6] += profiler.lap_seconds();
        }

        // Step 5: Launch kernel
        if (!use_shmem) {
            //timepoint = std::chrono::high_resolution_clock::now();

            dim3 dimGridHist2D((vectorLength + blkSize.x - 1) / blkSize.x, f_count);
            dim3 dimGridcMI((max_num_states + blkSize.x - 1) / blkSize.x, f_count);
            k_glob_hist2D_batch<D>
            <<<dimGridHist2D, blkSize, 0, stream>>>(device_data, target_id, d_task_feats_idxs, vectorLength,
                                                    d_data_num_states, d_ptr_data_sc, d_ptr_joint_sc);
            //cudaStreamSynchronize(stream);
            //times[6] += std::chrono::duration_cast<std::chrono::microseconds>(
            //        std::chrono::high_resolution_clock::now() - timepoint).count();
            //timepoint = std::chrono::high_resolution_clock::now();
            k_glob_cMI_batch<R>
            <<<dimGridcMI, blkSize, 0, stream>>>(d_ptr_data_sc, d_target_state_counts, d_ptr_joint_sc,
                                                 d_data_num_states,
                                                 target_num_states, d_joint_num_states, d_task_feats_idxs,
                                                 d_mutualInformation, (R) 1.0 / vectorLength,
                                                 device_lut_data, lut_bf);
            //times[7] += std::chrono::duration_cast<std::chrono::microseconds>(
            //        std::chrono::high_resolution_clock::now() - timepoint).count();
        } else {
            dim3 dimGrid(f_count);
            k_shared_hist2D_and_cMI_batch<D, R>
            <<<dimGrid, blkSize, shmem_size, stream>>>(device_data, target_id, d_task_feats_idxs, vectorLength,
                                                       d_data_num_states,
                                                       d_target_state_counts, target_num_states,
                                                       d_mutualInformation, (R) 1.0 / vectorLength,
                                                       device_lut_data, lut_bf);

        }

        if (profile && no_async) {
            times[7] += profiler.lap_seconds();
            cudaStreamSynchronize(stream);
            times[8] += profiler.lap_seconds();
        }

    }

    if (profile) {
        times[2] += profiler.lap_seconds();
    }

    // Copy results back
    cudaDeviceSynchronize();
    cudaError_t error = cudaGetLastError();
    if (error != cudaSuccess) {
        throw std::runtime_error(
                std::string(cudaGetErrorName(error)) + ": " + std::string(cudaGetErrorString(error)));
    }
    cudaMemcpy(mutualInformation, d_mutualInformation, numFeat * sizeof(R), cudaMemcpyDeviceToHost);

    if (profile) {
        times[3] += profiler.lap_seconds();
    }

    // Free memory
    for (cudaStream_t &s: streams) {
        cudaStreamDestroy(s);
    }

    cudaFree(d_data_num_states_pool);
    cudaFree(d_joint_num_states_pool);
    cudaFree(d_task_feats_idxs_pool);
    cudaFree(d_ptr_data_sc_pool);
    cudaFree(d_ptr_joint_sc_pool);
    cudaFreeHost(data_num_states_pool);
    cudaFreeHost(task_feats_idxs_pool);
    cudaFreeHost(d_data_state_counts_pool);
    cudaFreeHost(d_joint_state_counts_pool);
    cudaFree(d_dsc_pool);
    cudaFree(d_jsc_pool);
    delete[] target_state_counts;
    cudaFree(d_target_state_counts);
    cudaFree(d_mutualInformation);

    if (profile) {
        times[4] += profiler.lap_seconds();
        times[0] += Profiler::seconds_from(start);
    }
}

template<typename D, typename R>
void mrmr_base_lut(unsigned k, Dataset<D> const &dat, Results &results, Config &config, D *device_data,
                   uint16_t *device_lut_data, double lut_bf) {
    /* Array for time measurement */
    double times[16] = {0};

    /* MI values */
    R *classMI = new R[dat.num_feat]; // MI of each feature with class
    R *featureMIMatrix = new R[k * dat.num_feat]; // MI of each feature with selected

    /* Selected features map */
    auto *selectedFeatures = new bool[dat.num_feat];

    /* Cache for maxState of all features and class */
    auto *max_states = new unsigned int[dat.num_feat + 1];
    unsigned int max_state = 0, min_state = 2 << 16;
    // Compute maxState
    Profiler profiler;
    profiler.start();

#pragma omp parallel for default(none) shared(max_states, dat)
    for (unsigned int i = 0; i < dat.num_feat + 1; ++i) {
        max_states[i] = maxState(&dat.raw_data[i * dat.num_samp], dat.num_samp);
    }
#pragma omp parallel for default(none) shared(max_states, dat) reduction(min: min_state) reduction(max: max_state)
    for (unsigned int i = 0; i < dat.num_feat; ++i) {
        if (max_states[i] > max_state) {
            max_state = max_states[i];
        }
        if (max_states[i] < min_state) {
            min_state = max_states[i];
        }
    }

    if (config.doProfile()) {
        times[9] += profiler.lap_seconds();
    }

    /* Init variables */
    R maxMI = -1.0;
    unsigned int maxMICounter = 0;
    for (unsigned int i = 0; i < dat.num_feat; i++) {
        classMI[i] = 0.0;
        selectedFeatures[i] = false;
    }

    calcMutualInformationAll<D, R>(dat.raw_data, device_data, dat.num_feat, dat.num_samp, dat.num_feat, times, classMI,
                                   config, selectedFeatures, max_states, min_state, max_state, config.doProfile(),
                                   device_lut_data, lut_bf);
    for (unsigned int i = 0; i < dat.num_feat; i++) {
        if (classMI[i] > maxMI) {
            maxMI = classMI[i];
            maxMICounter = i;
        }/*if bigger than current maximum*/
    }/*for noOfFeatures - filling classMI*/

    selectedFeatures[maxMICounter] = true;
    results.push_back({maxMICounter, (double) maxMI});

    for (unsigned int i = 1; i < k; i++) {
        /****************************************************
        ** to ensure it selects some features
        **if this is zero then it will not pick features where the redundancy is greater than the
        **relevance
        ****************************************************/
        R score = -std::numeric_limits<R>::max();
        R totalFeatureMI;
        R currentScore = 0.0;
        unsigned int currentHighestFeature = 0;

        // First use kernel, then sum cached values
        calcMutualInformationAll<D, R>(dat.raw_data, device_data, results[i - 1].first, dat.num_samp, dat.num_feat,
                                       times, &featureMIMatrix[(i - 1) * dat.num_feat], config, selectedFeatures,
                                       max_states, min_state, max_state, config.doProfile(), device_lut_data, lut_bf);

        for (unsigned int j = 0; j < dat.num_feat; j++) {
            /*if we haven't selected j*/
            if (selectedFeatures[j] == 0) {
                currentScore = classMI[j];
                totalFeatureMI = 0.0;

                for (unsigned int x = 0; x < i; x++) {
                    // calcMI is only needed for the last selected (i-1) with all the not yet selected
                    totalFeatureMI += featureMIMatrix[x * dat.num_feat + j];
                }

                currentScore -= (totalFeatureMI / i);
                if (currentScore > score) {
                    score = currentScore;
                    currentHighestFeature = j;
                }
            }/*if j is unselected*/
        }/*for number of features*/

        selectedFeatures[currentHighestFeature] = true;
        results.push_back({currentHighestFeature, (double) score});

    }/*for the number of features to select*/

    if (config.doProfile()) {
        //config.appendProfileEntry(times[0] * 1e-3, "Algorithm execution");
        config.appendProfileEntry(times[9], "+-- Computation of Max States");
        config.appendProfileEntry(times[1], "|   Initial setup & Allocs");
        if (config.getNumStreams() > 0) {
            config.appendProfileEntry(times[2], "|   Quasi-Async work (transferences and kernels)");
        } else {
            config.appendProfileEntry(times[5], "|   CPU batch work");
            config.appendProfileEntry(times[6], "|   GPU Memcpy and memset");
            config.appendProfileEntry(times[7], "|   Launch kernels");
            config.appendProfileEntry(times[8], "|   Wait for kernels");
        }
        config.appendProfileEntry(times[3], "|   Wait for last kernel and copy results");
        config.appendProfileEntry(times[4], "|   Deallocs");
    }

    delete[] max_states;
    delete[] selectedFeatures;
    delete[] featureMIMatrix;
    delete[] classMI;
}

/****************************************************************************** With Lookup Table and fixed data type */

template<typename T>
void mrmr(unsigned k, Dataset<T> const &dat/*dataset*/, Results &results, Config &config) {

    // From now on, we suppose there is only 1 row for classes

    // Set k to min(k, num_feat)
    if (k > dat.num_feat) {
        k = dat.num_feat;
    }

    cudaSetDevice(config.getGpuIndex());
    checkCudaError();

    /* Set max memory size value */
    cudaDeviceProp properties{};
    cudaGetDeviceProperties(&properties, config.getGpuIndex());
    MAX_SHARED_MEMORY_PER_BLOCK = properties.sharedMemPerBlock;
    checkCudaError();

    /* Profiling with events */
    cudaEvent_t start, stop;
    if (config.doProfile()) {
        cudaEventCreate(&start);
        cudaEventCreate(&stop);
        checkCudaError();
    }

    /* LookUp Table */
    unsigned int lut_bf = 1;
    uint16_t *d_lut_data = nullptr;
    if (config.getPrecision() == Config::FIXED) {
        if (config.doProfile()) {
            cudaEventRecord(start);
        }

        LUT<uint16_t> h_lut(dat.num_samp);
        lut_bf = h_lut.getBF();

        cudaMalloc(&d_lut_data, dat.num_samp * sizeof(uint16_t));
        cudaMemcpy(d_lut_data, h_lut.getRawData(), dat.num_samp * sizeof(uint16_t), cudaMemcpyHostToDevice);
        //cudaMemcpyToSymbol(c_lut_data, h_lut.getRawData(), dat.num_samp * sizeof(uint16_t));

        if (config.doProfile()) {
            cudaEventRecord(stop);
            cudaEventSynchronize(stop);
            float ms;
            cudaEventElapsedTime(&ms, start, stop);
            checkCudaError();
            config.appendProfileEntry(ms * 1e-3, "Lookup Table creation & transfer");
        }
    }

    /* Alloc memory */
    T *d_dat;
    if (config.doProfile()) {
        cudaEventRecord(start);
    }
    cudaMalloc(&d_dat, dat.numBytes());
    if (config.doProfile()) {
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        float ms;
        cudaEventElapsedTime(&ms, start, stop);
        checkCudaError();
        config.appendProfileEntry(ms * 1e-3, "Memory alloc");
    }

    /* Copy data host -> device */
    if (config.doProfile()) {
        cudaEventRecord(start);
    }
    cudaMemcpy(d_dat, dat.raw_data, dat.numBytes(), cudaMemcpyHostToDevice);
    if (config.doProfile()) {
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        float ms;
        cudaEventElapsedTime(&ms, start, stop);
        checkCudaError();
        config.appendProfileEntry(ms * 1e-3, "Copy dataset Host->Device (" + std::to_string(dat.numBytes()) + " B)");
    }

    /* Launch kernel */
    if (config.doProfile()) {
        cudaEventRecord(start);
    }

    if (config.getPrecision() == Config::DOUBLE) {
        mrmr_base<T, double>(k, dat, results, config, d_dat);
    } else if (config.getPrecision() == Config::SINGLE) {
        mrmr_base<T, float>(k, dat, results, config, d_dat);
    } else if (config.getPrecision() == Config::FIXED) {
        mrmr_base_lut<T, double>(k, dat, results, config, d_dat, d_lut_data, 1.0 / (1 << lut_bf));
    }

    if (config.doProfile()) {
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        float ms;
        cudaEventElapsedTime(&ms, start, stop);
        checkCudaError();
        config.appendProfileEntry(ms * 1e-3, "Algorithm execution");
    }

    /* Free memory */
    if (config.doProfile()) {
        cudaEventRecord(start);
    }
    if (config.getPrecision() == Config::FIXED) {
        cudaFree(d_lut_data);
    }
    cudaFree(d_dat);
    if (config.doProfile()) {
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        float ms;
        cudaEventElapsedTime(&ms, start, stop);
        checkCudaError();
        config.appendProfileEntry(ms * 1e-3, "Memory dealloc");
    }

    /* Free events */
    if (config.doProfile()) {
        cudaEventDestroy(stop);
        cudaEventDestroy(start);
        checkCudaError();
    }
}

template<>
void mrmr(unsigned k, Dataset<unsigned> const &dataset, Results &results) {
    Config default_config;
    return mrmr<unsigned>(k, dataset, results, default_config);
}

template<>
void mrmr(unsigned k, Dataset<uint16_t> const &dataset, Results &results) {
    Config default_config;
    return mrmr<uint16_t>(k, dataset, results, default_config);
}