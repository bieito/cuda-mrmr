#ifndef CUDA_MRMR_MRMR_CUH
#define CUDA_MRMR_MRMR_CUH

#include <limits>
#include <cstring>
#include <chrono>

#include "Profiler/Profiler.h"
#include "lut.h"

#define LOG_BASE 2.0

#endif //CUDA_MRMR_MRMR_CUH
