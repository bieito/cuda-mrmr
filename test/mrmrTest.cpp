#include <iostream>

extern "C" {
#include "FEAST/FSAlgorithms.h"
}

#include "cuda-mrmr/mrmr.h"
#include "TestTools/TestTools.h"


Test(cudaMrmrValidationSmall) {

    const unsigned num_samples = 4, num_feats = 5, num_classes = 1, k = 5;
    unsigned dataset[(num_feats + num_classes) * num_samples] = {1, 1, 1, 1,  // feature major
                                                                 0, 0, 0, 0,
                                                                 1, 0, 0, 0,
                                                                 0, 1, 0, 1,
                                                                 0, 1, 1, 1,
                                                                 0, 0, 1, 1}; // last is class

    // FEAST
    unsigned *f_mat[num_feats];
    unsigned *class_col;
    double f_scores[k];
    unsigned f_out[k];

    for (unsigned i = 0; i < num_feats; ++i) {
        f_mat[i] = &dataset[i * num_samples];
    }
    class_col = &dataset[num_feats * num_samples];

    mRMR_D(k, num_samples, num_feats, f_mat, class_col, f_out, f_scores);

    // CUDA
    Dataset<unsigned> dat;
    dat.copy(dataset, num_feats, num_samples, num_classes);
    Results res;
    mrmr(k, dat, res);

    // ERROR CHECK
    const double TOL = 10e-6;
    for (unsigned i = 0; i < k; ++i) {
        AssertEquals(f_out[i], res[i].first);
        AssertTrue((f_scores[i] - res[i].second) < TOL,
                   "At result " + to_string(i) + ": " + to_string(f_scores[i]) + " != " + to_string(res[i].second));
    }

}

Test(cudaMrmrValidationRandom) {

    const unsigned num_samples = 128, num_feats = 96, num_classes = 1, k = 96;
    unsigned dataset[(num_feats + num_classes) * num_samples];
    for (unsigned int &i : dataset) {
        i = (unsigned) (rand() % 64);
    }

    // FEAST
    unsigned *f_mat[num_feats];
    unsigned *class_col;
    double f_scores[k];
    unsigned f_out[k];

    for (unsigned i = 0; i < num_feats; ++i) {
        f_mat[i] = &dataset[i * num_samples];
    }
    class_col = &dataset[num_feats * num_samples];

    mRMR_D(k, num_samples, num_feats, f_mat, class_col, f_out, f_scores);

    // CUDA
    Dataset<unsigned> dat;
    dat.copy(dataset, num_feats, num_samples, num_classes);
    Results res;
    mrmr(k, dat, res);

    // ERROR CHECK
    /* This would be the perfect test, but there can occur permutations due to precision errors
     * However, if the last selected feature is equal, it is likely to be a valid result */
    /* for (unsigned i = 0; i < k; ++i) {
        AssertTrue(f_out[i] == res[i].first,
                   "At result " + to_string(i) + ": " + to_string(f_out[i]) + " != " + to_string(res[i].first));
        AssertTrue((f_scores[i] - res[i].second) < TOL,
                   "At result " + to_string(i) + ": " + to_string(f_scores[i]) + " != " + to_string(res[i].second));
    }*/
    const double TOL = 1e-6;
    AssertEquals(f_out[k - 1], res[k - 1].first);
    AssertTrue((f_scores[k - 1] - res[k - 1].second) < TOL,
               "ERROR : " + to_string(f_scores[k - 1]) + " != " + to_string(res[k - 1].second));

}

Test(datasetDoubleAlloc) {
    Dataset<char> mydata;
    mydata.initAllocate(16, 8, 32);

    AssertIsThrown(runtime_error, {
        mydata.allocate();
    })
}

int main(/*int argc, char *argv[]*/) {

    //TestManager::setVerbosityLevel(TestManager::FULL);

    AddTest(cudaMrmrValidationSmall);
    AddTest(cudaMrmrValidationRandom);
    AddTest(datasetDoubleAlloc);

    return TestManager::runTests();
}
